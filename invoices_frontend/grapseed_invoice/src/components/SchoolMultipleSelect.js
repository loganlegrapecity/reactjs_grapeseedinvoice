import React, { useState, useEffect, useContext } from 'react';
import * as wjInput from '@grapecity/wijmo.react.input';
import '@grapecity/wijmo.styles/wijmo.css';
import { GlobalContext, GenerateInvoiceParametersReducerAction } from '../contexts/GlobalContext';

const SchoolMultipleSelect = (props) => {

    // const [dataSource, setDataSource] = useState(); 
    const { generateInvoiceParametersDispatch } = useContext(GlobalContext);

    const onSelectedItemsChanged = (sender) => {
        console.log(sender.selectedItems);
        generateInvoiceParametersDispatch({ type: GenerateInvoiceParametersReducerAction.SET_SELECTED_SCHOOLS, selectedSchools: sender.selectedItems });
    };

    // có thể sử dụng để load dữ liệu rồi set vào state ở trên
    // nhưng trong ví dụ này thì dữ liệu được truyền từ ngoài vào thông qua props
    useEffect(() => {
        // get school from storage
    }, []);

    return (
        <div> 
            <wjInput.MultiAutoComplete placeholder="Select school"
                displayMemberPath="name"
                selectedItemsChanged={onSelectedItemsChanged}
                itemsSource={props.schools}>
            </wjInput.MultiAutoComplete>
        </div>
    )
}

export default SchoolMultipleSelect