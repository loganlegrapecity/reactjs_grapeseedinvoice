import axios from 'axios';
import swal from "sweetalert";
import React, { Component } from 'react';
import { standardApi } from '../helper/apiHelper';
import { showAlert, loadingMask } from '../helper/util';

export default class UploadFile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedFile: null
        };
    }

    onFileChange = event => {
        this.setState({ selectedFile: event.target.files[0] });
    };

    onFileUpload = () => {

        if ((this.props.fileType + "").indexOf(this.state.selectedFile.type) < 0) {
            showAlert('File type is not valid');
            return;
        }

        if (!this.state.selectedFile) {
            showAlert("You have to select a file before uploading!");
            return;
        }

        const formData = new FormData();
        formData.append(
            "myFile",
            this.state.selectedFile,
            this.state.selectedFile.name
        );

        if (this.state.selectedFile.size / 1024 > 2000) {
            swal({
                title: "Warning!",
                text: "Max size is 2MB",
                icon: "warning",
                button: "Close",
            });
            return;
        }

        loadingMask();
        let postApi = this.props.postApi ? this.props.postApi : "api/upload-file/UploadInvoiceTemplate";
        axios.post(standardApi(postApi), formData).then(function (response) {
            if (response.data.success) {
                swal({
                    title: "Success!",
                    text: "Upload file successfully",
                    icon: "success",
                    button: "Close",
                });
            }
            loadingMask();
        });
    };

    render() {

        return (
            <div>
                <input type="file"
                    onChange={this.onFileChange}
                    accept={this.props.fileType} />
                <button className="btn-grape" onClick={this.onFileUpload}>
                    Upload
                    </button>
            </div>
        );
    }
}