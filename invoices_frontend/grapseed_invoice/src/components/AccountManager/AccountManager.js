import React, { useState, useEffect } from 'react';
import './account-manager.scss';
import { FaCheck } from "react-icons/fa";
import { FaTimes } from "react-icons/fa";
import { shortMe } from '../../helper/stringHelper';
import AccountManagerService from '../../services/AccountManagerService';
import { useToasts } from 'react-toast-notifications';

const AccountManager = (props) => {

    const [active, setActive] = useState(true);
    const { addToast } = useToasts();
    const renderIcon = () => {
        if (active) {
            return (
                <span className="status-icon text-success">
                    <FaCheck />
                    <small>Included</small>
                </span>
            )
        }

        return (
            <span className="status-icon text-danger">
                <FaTimes />
                <small>Excluded</small>
            </span>
        )
    }

    useEffect(() => {
        setActive(props.status);
    }, []);

    const toggleStatus = () => {
        const newStatus = !active;
        setActive(newStatus);
        const model = {
            id: props.id,
            name: props.fullName,
            email: props.email,
            isIncludedInReport: newStatus
        };

        new AccountManagerService().saveAccountManager(model, function (response) {
            addToast('Successfully', {
                appearance: 'success',
                autoDismiss: true,
            })
        })
    }

    const getShortName = (fullName) => {
        if (!fullName || fullName.trim().length === 0) {
            return "N/A";
        }
        fullName = fullName.trim();
        if (fullName.length === 1)
            return fullName;

        if (fullName.length > 1 && fullName.indexOf(' ') < 0) {
            return fullName[0] + fullName[1];
        }

        const n = fullName.split(' ');
        let name = "";
        n.map(v => {
            if (v.trim().length != 0 && name.length < 2) {
                name += v.trim()[0]
            }
        });

        return name;
    }

    const shortEmail = (email) => {
        return shortMe(email, 20);
    }

    return (
        <div className={active ? "account-manager" : "account-manager remove"} onClick={() => toggleStatus()}
            title={active ? 'included in the invoice' : 'excluded from the invoice'}>
            <div className="short-name">{getShortName(props.fullName)}</div>
            <p className="name">{props.fullName}</p>
            <small className="email text-muted" title={props.email}>{shortEmail(props.email)}</small>
            {renderIcon()}
        </div>
    )
}

export default AccountManager