import React from 'react';
import { NavLink } from "react-router-dom";
import AuthenticationService from '../../services/AuthenticationService';
import swal from 'sweetalert';
import './navigation-style.scss';

const Navigation = () => {
    const signout = () => {
        swal({
            title: "Confirm to sign out?",
            text: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willSignOut) => {
                if (willSignOut) {
                    new AuthenticationService().signOut();
                    window.location.reload();
                } else {
                    //swal("Your imaginary file is safe!");
                }
            });
    }

    return (
        <div id="top-navigation" className="clear">
            <div id="logo">
                <NavLink exact to="/generate-invoices">Logo</NavLink>
            </div>
            <nav className="navbar navbar-expand-sm">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink to="/generate-invoices" className="nav-link">Generate invoices</NavLink>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Settings</a>
                        <div className="dropdown-menu">
                            <NavLink className="dropdown-item" to="/price-setting">
                                Invoice setting
                            </NavLink>
                            <NavLink className="dropdown-item" to="/excluded-account-manager">Excluded account managers</NavLink>
                        </div>
                    </li>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                            Template
                        </a>
                        <div className="dropdown-menu">
                            <NavLink className="dropdown-item" to="/setting-invoice-template">Invoice templates</NavLink>
                            <NavLink className="dropdown-item" to="/spreadsheet-setting">Vietnam license file</NavLink>
                        </div>
                    </li>
                    <li className="nav-item">
                        <a onClick={() => signout()} className="nav-link">SIGN OUT</a>
                    </li>
                </ul>
            </nav>
        </div>
    )
}

export default Navigation