import React from 'react';
import logo from './logo.svg';
import './styles/App.scss';
import { changeBodyClass, isValidToken } from './helper/util';
import {
  BrowserRouter,
  Switch,
  withRouter,
  Route,
  Link,
  NavLink,
  useParams
} from "react-router-dom";
import Login from './pages/login/login';
import Navigation from './components/TopMenu/Navigation';
import GlobalContextProvider from './contexts/GlobalContext';
import GenerateInvoices from './pages/generate_form/GenerateInvoices';
import InvoiceTemplate from './pages/settings/InvoiceTemplate';
import ExcludedAccountManager from './pages/settings/ExcludedAccountManager';
import { ToastProvider } from 'react-toast-notifications';
import PriceSettingPage from './pages/settings/PriceSettingPage';
import VietnamLicenseReportSetting from './pages/settings/VietnamLicenseReportSetting';

function App() {

  if (!isValidToken()) {
    changeBodyClass('login-page');
    return (
      <BrowserRouter>
        <Login></Login>
      </BrowserRouter>
    )
  }

  return (
    <GlobalContextProvider>
      <ToastProvider>
        <div className="container">
          <BrowserRouter>
            <Navigation></Navigation>
            <div id="main-body">
              <Switch>
                <Route exact path="/login">
                  <Login></Login>
                </Route>
                <Route exact path="/">
                  <GenerateInvoices></GenerateInvoices>
                </Route>
                <Route exact path="/generate-invoices">
                  <GenerateInvoices></GenerateInvoices>
                </Route>
                <Route exact path="/price-setting">
                  <PriceSettingPage></PriceSettingPage>
                </Route>
                <Route exact path="/excluded-account-manager">
                  <ExcludedAccountManager></ExcludedAccountManager>
                </Route>
                <Route exact path="/setting-invoice-template">
                  <InvoiceTemplate></InvoiceTemplate>
                </Route>
                <Route exact path="/spreadsheet-setting">
                  <VietnamLicenseReportSetting></VietnamLicenseReportSetting>
                </Route>
              </Switch>
            </div>
          </BrowserRouter>
        </div>
      </ToastProvider>
    </GlobalContextProvider>
  );
}

export default App;
