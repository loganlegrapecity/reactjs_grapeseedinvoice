import { ApiConst } from "../consts/ApiConst";
import { standardApi } from "../helper/apiHelper";
import axios from 'axios';
import { loadingMask } from "../helper/util";

export default class AuthenticationService {
    signIn(email, password) {

        loadingMask();
        axios.post(standardApi(ApiConst.LoginApi), { email, password })
            .then(function (response) {
                loadingMask();
                // login success
                if (response.data.access_token && response.data.access_token.length) {
                    localStorage.setItem("token", response.data.access_token);
                    window.location.href = '/';
                } else {
                    // login fail
                    alert('Login failure');
                }
            })
    }

    signOut() {
        localStorage.removeItem("token");
        localStorage.removeItem("schools");
        window.location.reload();
    }
}