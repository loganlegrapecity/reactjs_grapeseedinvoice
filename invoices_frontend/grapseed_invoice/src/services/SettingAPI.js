import { apiGet, apiPost, standardApi } from "../helper/apiHelper";
import { isValidToken } from "../helper/util";
import { ApiConst } from "../consts/ApiConst";
import AuthenticationService from "./AuthenticationService";

export default class SettingAPI {
    constructor() {
        if (!isValidToken()) {
            new AuthenticationService().signOut();
        }
    }

    //template file
    getInvoiceTemplateFile = (callback) => {
        apiGet(ApiConst.GetTemplateFilePath, callback);
    }

    // template cell setting
    getTemplateCellSetting = (callback) => {
        apiGet(ApiConst.GetInvoiceTemplateCellSetting, callback);
    }

    saveTemplateCellSetting = (model, callback) => {
        apiPost(ApiConst.SaveInvoiceTemplateCellSetting, model, callback);
    }

    resetTemplateCellSetting = (callback) => {
        apiGet(ApiConst.ResetInvoiceTemplateCellSetting, callback);
    }

    //price setting
    savePriceSetting = (model, callback) => {
        apiPost(standardApi(ApiConst.SavePriceSetting), model, callback);
    }

    getPriceSetting = (callback) => {
        apiGet(ApiConst.GetPriceSetting, callback);
    }

    // vietnam license setting
    saveVietnamLicenseSetting = (model, callback) => {
        apiPost(ApiConst.SaveVietnamLicenseReportSetting, model, callback);
    }

    getVietnamLicenseSetting = (callback) => {
        apiGet(ApiConst.GetVietnamLicenseReportSetting, callback);
    }

    resetVietnamLicenseSetting = (callback) => {
        apiPost(ApiConst.ResetVietnamLicenseReportSetting, null, callback);
    }
}