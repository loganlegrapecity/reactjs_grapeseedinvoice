import GrapeSeedCommonAPI from "./GrapeSeedCommonAPI"
import swal from "sweetalert";


export default class DatasourceService {
    process = (generateInvoiceParameters) => {
        const accountIds = [];
        const accountManagers = [];
        let orderedMaterialProducts = [];
        let productsListInDatabase = [];
        const schools = JSON.parse(localStorage.getItem("schools")).schools;
        const apiCaller = new GrapeSeedCommonAPI();

        // get all accountIds
        if (!schools || schools.length === 0) {
            swal({
                title: "Warning!",
                text: "There is no schools data",
                icon: "warning",
                button: "Close",
            });
            return;
        }
        schools.map((s) => {
            if (s && s.accountManagerIds && s.accountManagerIds.length) {
                s.accountManagerIds.map((id) => {
                    if (accountIds.indexOf(id) < 0) {
                        accountIds.push(id);
                    }
                });
            }
        });

        // get accounts
        apiCaller.getAccountManagers(accountIds, function (response) {
            const accounts = response.data;
            if (accounts && accounts.length) {
                accounts.map((a) => {
                    accountManagers.push({
                        id: a.id,
                        name: a.name,
                        email: a.email
                    })
                });
            }
        });

        //get material products
        apiCaller.getMaterialProducts(function (response) {
            orderedMaterialProducts = response.data;
            if (orderedMaterialProducts?.length) {
                apiCaller.getProducts(function (products) {
                    productsListInDatabase = products.data;
                });
            }
        });
    }
}