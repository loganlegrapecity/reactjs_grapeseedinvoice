import { ApiConst } from "../consts/ApiConst";
import AuthenticationService from "./AuthenticationService";
import { apiGet, apiPost } from "../helper/apiHelper";
import { isValidToken } from "../helper/util";

export default class AccountManagerService {
    constructor() {
        if (!isValidToken()) {
            new AuthenticationService().signOut();
        }
    }

    getAccountManagerIds = () => {
        const accountIds = [];
        const schools = JSON.parse(localStorage.getItem("schools")).schools;
        schools.map((s) => {
            if (s && s.accountManagerIds && s.accountManagerIds.length) {
                s.accountManagerIds.map((id) => {
                    if (accountIds.indexOf(id) < 0) {
                        accountIds.push(id);
                    }
                });
            }
        });
        return accountIds;
    }

    getAccountManagers = (accountIds, callback) => {
        const Ids = accountIds.join();
        apiGet(ApiConst.GetAccountManagers + "?accountManagerIds=" + Ids, callback);
    }

    synchronizeData = (accountIds, callback) => {
        const Ids = accountIds.join();
        apiGet(ApiConst.SynchronizeAccountManagers + "?accountManagerIds=" + Ids, callback);
    }

    saveAccountManager = (model, callback) => {
        apiPost(ApiConst.ToggleIncluded, model, callback, true);
    }
}