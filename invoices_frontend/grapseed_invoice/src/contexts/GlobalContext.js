import React, { createContext, useReducer } from 'react';
export const GlobalContext = createContext();

export const GenerateInvoiceParametersReducerAction = {
    SET_SELECTED_SCHOOLS: 'SET_SELECTED_SCHOOLS'
}

const generateInvoiceParametersReducer = (state, action) => {
    switch (action.type) {
        case GenerateInvoiceParametersReducerAction.SET_SELECTED_SCHOOLS: {
            //return { ...state, selectedSchools: action.selectedSchools };
            return state;
        }
        // case 'ADD_NUM':
        //     return state + action.num;

        default:
            return state;
    }
}

const GlobalContextProvider = (props) => {

    const [generateInvoiceParameters, generateInvoiceParametersDispatch] = useReducer(generateInvoiceParametersReducer, {});

    return (
        <GlobalContext.Provider value={{ generateInvoiceParameters, generateInvoiceParametersDispatch }}>
            {props.children}
        </GlobalContext.Provider>
    )
}

export default GlobalContextProvider