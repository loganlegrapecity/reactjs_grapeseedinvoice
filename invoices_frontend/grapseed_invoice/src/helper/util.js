import $ from 'jquery';
import swal from 'sweetalert';

export function loadingMask() {
    if ($(".loading-mask").length) {
        $(".loading-mask").remove();
    } else {
        $("body").append("<div class='loading-mask'></div>")
    }
}

export function changeBodyClass(className) {
    $("body").addClass(className);
}

export function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};

export function isValidToken() {
    const token = localStorage.getItem("token");
    if (!token)
        return false;

    const obj = parseJwt(token);
    const expireDate = obj.exp;
    return new Date(expireDate * 1000) > new Date();
}

// dialog
export function showAlert(message) {
    swal({
        title: "Warning!",
        text: message,
        icon: "warning",
        button: "Close"
    });
}

export function showSuccess(message){
    swal({
        title: "Success!",
        text: message,
        icon: "success",
        button: "OK"
    });
}