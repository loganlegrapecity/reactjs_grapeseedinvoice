export function extractNumber(input) {
    const str = input + "";
    if (!str || str.trim().length === 0) {
        return undefined;
    }

    let number = '';
    for (let i = 0; i < str.length; i++) {
        if (!isNaN(str[i])) {
            number += str[i];
        }
    }

    return number * 1;
}

export function extractCharacters(input) {
    const str = input + "";
    if (!str || str.trim().length === 0) {
        return '';
    }

    let text = '';
    for (let i = 0; i < str.length; i++) {
        if (isNaN(str[i])) {
            text += str[i];
        }
    }

    return text;
}

export function shortMe(input, maxLength = 100) {
    if (!input)
        return input;

    if (input.length <= maxLength)
        return input;

    return input.trim().substring(0, maxLength) + "...";
}