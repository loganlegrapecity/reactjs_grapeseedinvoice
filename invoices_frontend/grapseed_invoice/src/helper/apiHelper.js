import axios from 'axios';
import { loadingMask, showAlert } from './util';
import { RootApi } from '../consts/ApiConst';

export function apiPost(apiUrl, model, callbackFunction, turnOffLoading = false) {
    if (!turnOffLoading) {
        loadingMask();
    }

    axios({
        method: 'post',
        url: standardApi(apiUrl),
        data: model,
        headers: {
            'Auth': localStorage.getItem("token")
        }
    }).then(function (response) {
        if (!turnOffLoading) {
            loadingMask();
        }
        if (response.data.success) {
            if (callbackFunction)
                callbackFunction(response.data);
        } else {
            // error
            showAlert(response.data.message);
        }
    });
}

export function apiGet(apiUrl, callbackFunction, turnOffLoading = false) {
    if (!turnOffLoading) {
        loadingMask();
    }

    axios({
        method: 'get',
        url: standardApi(apiUrl),
        headers: {
            'Auth': localStorage.getItem("token")
        }
    }).then(function (response) {
        if (!turnOffLoading) {
            loadingMask();
        }
        if (response.data.success) {
            if (callbackFunction)
                callbackFunction(response.data);
        } else {
            // error
            showAlert(response.data.message);
        }
    });
}

export function standardApi(api) {
    if (api.startsWith(RootApi)) {
        return api;
    }

    if (!RootApi.endsWith("/") && !api.startsWith("/")) {
        return RootApi + "/" + api;
    }

    return RootApi + api;
}