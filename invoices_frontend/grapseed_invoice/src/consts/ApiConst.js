export const RootApi = "https://localhost:44303";

//API URL
export const ApiConst = {
    //grapeschool API
    LoginApi: "/api/authentication/sign-in",
    AdminApi: "/api/grapeseed/accounts",
    SchoolApi: '/api/grapeseed/schools',
    MaterialProducts: "/api/grapeseed/material-products",
    Products: "/api/grapeseed/products",
    ProductPrice: "https://services.grapeseed.com/admin/v1/products/local/{0}",
    CampusGsNumber: "https://services.grapeseed.com/admin/v1/schools/{0}/campuses?offset=0&limit=100&disabled=false",

    // setting
    GetInvoiceTemplateCellSetting: '/api/setting/cell-configuration',
    SaveInvoiceTemplateCellSetting: '/api/setting/save-cell-configuration',
    ResetInvoiceTemplateCellSetting: '/api/setting/reset-cell-configuration',

    // Account mananger
    GetAccountManagers: '/api/account-manager/all',
    SynchronizeAccountManagers: '/api/account-manager/synchronize-data',
    ToggleIncluded: '/api/account-manager/toggle-included',

    //Invoice template
    GetTemplateFilePath: '/api/setting/invoice-template',

    //price setting
    SavePriceSetting: '/api/setting/save-price-setting',
    GetPriceSetting: '/api/setting/price-setting',

    //vietnam license
    GetVietnamLicenseReportSetting: 'api/setting/license-report-setting',
    SaveVietnamLicenseReportSetting: 'api/setting/save-license-report-setting',
    ResetVietnamLicenseReportSetting: 'api/setting/reset-license-report-setting'
}