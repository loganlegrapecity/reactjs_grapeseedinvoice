import React, { useState, useEffect } from 'react';
import SettingAPI from '../../services/SettingAPI';
import { showSuccess } from '../../helper/util';

const PriceSettingPage = (props) => {
    const [littleSEEDLicense, setLittleSEEDLicense] = useState(0);
    const [grapeSEEDLicense, setGrapeSEEDLicense] = useState(0);
    const [excludeMaterialProducts, setExcludeMaterialProducts] = useState(false);

    useEffect(() => {
        new SettingAPI().getPriceSetting(function (response) { 
            setLittleSEEDLicense(response.data.littleSEEDLicense);
            setGrapeSEEDLicense(response.data.grapeSEEDLicense);
            setExcludeMaterialProducts(response.data.excludeMaterialProducts);
        });
    }, []);

    const handleSaveForm = (e) => {
        e.preventDefault();
        let priceSettings = {
            littleSEEDLicense: littleSEEDLicense,
            grapeSEEDLicense: grapeSEEDLicense,
            excludeMaterialProducts: excludeMaterialProducts
        }
        new SettingAPI().savePriceSetting(priceSettings, function (response) {
            showSuccess('Save successfully!');
        });
    }

    return (
        <div className="container">
            <div className="p-5 bg-white rounded shadow mb-5">
                <h1 className="box-header">
                    Invoice setting
                </h1>
                <form onSubmit={handleSaveForm}>
                    <div className="form-group">
                        <label>LittleSEED license price/year</label>
                        <input type="text"
                            className="form-control"
                            value={littleSEEDLicense}
                            onChange={(e) => setLittleSEEDLicense(e.target.value)}
                            placeholder="Enter price" required />
                    </div>
                    <div className="form-group">
                        <label>GrapeSEED license price/year</label>
                        <input type="text"
                            className="form-control"
                            value={grapeSEEDLicense}
                            onChange={(e) => setGrapeSEEDLicense(e.target.value)}
                            placeholder="Enter price" required />
                    </div>
                    <div className="form-check">
                        <input type="checkbox"
                            className="form-check-input"
                            checked={excludeMaterialProducts}
                            onChange={(e) => setExcludeMaterialProducts(e.target.checked)}
                            id="chk-exclude-material-products" />
                        <label className="form-check-label" htmlFor="chk-exclude-material-products">
                            Exclude material products from invoices
                        </label>
                    </div>
                    <div className="row mt-4">
                        <div className="col-sm-12 col-md-4 offset-md-4">
                            <button type="submit" className="btn-grape btn-block">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default PriceSettingPage