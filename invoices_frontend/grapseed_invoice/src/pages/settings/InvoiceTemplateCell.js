import React, { useState, useEffect } from 'react';
import SettingAPI from '../../services/SettingAPI';
import { extractNumber, extractCharacters } from '../../helper/stringHelper';
import { showSuccess } from '../../helper/util';

const InvoiceTemplateCell = (props) => {

    const [invoiceDate, setInvoiceDate] = useState('');
    const [schoolName, setSchoolName] = useState('');
    const [address, setAddress] = useState('');
    const [usdExchangeRate, setUsdExchangeRate] = useState('');
    const [startRowData, setStartRowData] = useState(0);
    const [productColumn, setProductColumn] = useState('');
    const [quantity, setQuantity] = useState('');
    const [adjustment, setAdjustment] = useState('');
    const [price, setPrice] = useState('');
    const [total, setTotal] = useState('');
    const [sheetName, setSheetName] = useState('');

    const submitCellConfiguration = (e) => {
        e.preventDefault();

        const model = {
            invoiceDateRow: extractNumber(invoiceDate),
            invoiceDateColumn: extractCharacters(invoiceDate),
            addressColumn: extractCharacters(address),
            addressRow: extractNumber(address),
            adjustmentColumn: extractCharacters(adjustment),
            exchangeRateColumn: extractCharacters(usdExchangeRate),
            exchangeRateRow: extractNumber(usdExchangeRate),
            priceColumn: extractCharacters(price),
            productColumn: extractCharacters(productColumn),
            quantityColumn: extractCharacters(quantity),
            schoolColumn: extractCharacters(schoolName),
            schoolRow: extractNumber(schoolName),
            sheetName: sheetName,
            startFillDataRow: extractNumber(startRowData),
            totalPriceColumn: extractCharacters(total),
        };
        new SettingAPI().saveTemplateCellSetting(model, function (response) {
            if (response.success) {
                showSuccess('Update successfully');
            }
            // console.error(response);
        });
    }

    const fetchSetting = () => {
        new SettingAPI().getTemplateCellSetting(function (response) {
            const setting = response.data;
            if (setting) {
                setInvoiceDate(setting.invoiceDateRow + setting.invoiceDateColumn);
                setSchoolName(setting.schoolRow + setting.schoolColumn);
                setAddress(setting.addressRow + setting.addressColumn);
                setUsdExchangeRate(setting.exchangeRateRow + setting.exchangeRateColumn);
                setStartRowData(setting.startFillDataRow);
                setProductColumn(setting.productColumn);
                setQuantity(setting.quantityColumn);
                setAdjustment(setting.adjustmentColumn);
                setPrice(setting.priceColumn);
                setTotal(setting.totalPriceColumn);
                setSheetName(setting.sheetName);
            }
        });
    }

    const resetToDefault = () => {
        new SettingAPI().resetTemplateCellSetting(function (response) {
            if (response.success) {
                fetchSetting();
                showSuccess('reset successfully!');
            }
        });

    }

    useEffect(() => {
        fetchSetting();
    }, []);

    return (
        <form onSubmit={submitCellConfiguration}>
            <div className="row">
                <div className="col">
                    <div className="form-group">
                        <label>Ngày hóa đơn (Invoice Date)</label>
                        <input type="text"
                            maxLength="2"
                            value={invoiceDate} onChange={(e) => setInvoiceDate(e.target.value)}
                            className="form-control" required />
                    </div>
                    <div className="form-group">
                        <label>Tên trường (School name)</label>
                        <input type="text" maxLength="2" className="form-control" value={schoolName} onChange={(e) => setSchoolName(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label>Địa chỉ (School address)</label>
                        <input type="text" maxLength="2" className="form-control" value={address} onChange={(e) => setAddress(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label>Tỷ giá</label>
                        <input type="text" className="form-control" value={usdExchangeRate} onChange={(e) => setUsdExchangeRate(e.target.value)} required />
                    </div>
                </div>
                <div className="col">
                    <div className="form-group">
                        <label>Dòng bắt đầu chèn sản phẩm</label>
                        <input type="text" maxLength="3" className="form-control" value={startRowData} onChange={(e) => setStartRowData(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label>Cột tên hàng hóa</label>
                        <input type="text" maxLength="1" className="form-control" value={productColumn} onChange={(e) => setProductColumn(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label>Cột số lượng</label>
                        <input type="text" maxLength="1" className="form-control" value={quantity} onChange={(e) => setQuantity(e.target.value)} required />
                    </div>
                </div>
                <div className="col">
                    <div className="form-group">
                        <label>Cột hiệu chỉnh</label>
                        <input type="text" maxLength="1" className="form-control" value={adjustment} onChange={(e) => setAdjustment(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label>Cột đơn giá</label>
                        <input type="text" maxLength="1" className="form-control" value={price} onChange={(e) => setPrice(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label>Cột tổng VNĐ</label>
                        <input type="text" maxLength="1" className="form-control" value={total} onChange={(e) => setTotal(e.target.value)} required />
                    </div>
                    <div className="form-group">
                        <label>Sheet name in excel file</label>
                        <input type="text" className="form-control" value={sheetName} onChange={(e) => setSheetName(e.target.value)} required />
                    </div>
                </div>
            </div>

            <div className="text-center mt-2">
                <button type="button" className="btn-grape-white mr-3" onClick={() => resetToDefault()}>Reset to default</button>
                <button type="submit" className="btn-grape btn-long">Save</button>
            </div>
        </form>
    )
}

export default InvoiceTemplateCell