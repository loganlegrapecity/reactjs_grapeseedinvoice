import React, { useState, useEffect } from 'react';
import UploadFile from '../../components/UploadFile';
import InvoiceTemplateCell from './InvoiceTemplateCell';
import SettingAPI from '../../services/SettingAPI';

const InvoiceTemplate = (props) => {
    const [uploadedPath, setUploadedPath] = useState('');
    const uploadFile = () => {
        return (
            <div>
                {
                    uploadedPath && <div className="text-info d-block mt-4 alert alert-info mb-3">
                        There is an existing file template. Click <a href={uploadedPath}>here</a> to download
                    </div>
                }
                <UploadFile fileType="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"></UploadFile>
                <small className="text-muted d-block mt-3">
                    If you don't have the invoice template yet, pls go to this
                <a target="_blank" href="https://grapecity.sharepoint.com/:x:/r/teams/grapeseed/vn/sales/Shared%20Documents/Templates/TEMPLATEMonthlyInvoice_19_03_2020.xlsx?d=w458196fa29254220814083d05c5c1ff5&csf=1&e=PCWDW9"> link</a> to download
                </small>
            </div>
        )
    }

    useEffect(() => {
        new SettingAPI().getInvoiceTemplateFile(function (response) {
            if (response.success)
                setUploadedPath(response.data);
        })
    });

    return (
        <div className="p-5 bg-white rounded shadow mb-5 mt-5">
            <h1 className="box-header">
                Invoice template
            </h1>
            <div>
                <ul className="nav nav-tabs">
                    <li className="nav-item">
                        <a className="nav-link active" data-toggle="tab" href="#home">Upload template file</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" data-toggle="tab" href="#menu1">Cell configurations</a>
                    </li>
                </ul>
                <div className="tab-content">
                    <div className="tab-pane container active" id="home">
                        {uploadFile()}
                    </div>
                    <div className="tab-pane container fade" id="menu1">
                        <InvoiceTemplateCell></InvoiceTemplateCell>
                    </div>
                </div>
            </div>



        </div>
    )
}

export default InvoiceTemplate