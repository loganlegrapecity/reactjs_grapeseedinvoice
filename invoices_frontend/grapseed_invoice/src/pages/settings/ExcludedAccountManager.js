import React, { useEffect, useState } from 'react';
import AccountManager from '../../components/AccountManager/AccountManager';
import AccountManagerService from '../../services/AccountManagerService';

const ExcludedAccountManager = (props) => {
    const [accountManagers, setAccountManagers] = useState([]);
    const renderAccountManagers = () => {

        if (accountManagers && accountManagers.length === 0) {
            return (
                <div className="alert alert-danger">
                    There is no account managers. plese try again by using the fetch data button
                </div>
            )
        }

        return (
            <div className="row">
                {
                    accountManagers.map((v, k) => (
                        <div key={k} className="col-sm-4 mb-3">
                            <AccountManager id={v.id} fullName={v.name} email={v.email} status={v.isIncludedInReport}></AccountManager>
                        </div>
                    ))
                }
            </div>
        )
    }

    useEffect(() => {
        const accountManagerService = new AccountManagerService();
        const accountIds = accountManagerService.getAccountManagerIds();
        accountManagerService.getAccountManagers(accountIds, function (response) {
            if (response.success) {
                setAccountManagers(response.data);
            }
        });
    }, []);

    const synchornizeData = () => {
        const accountManagerService = new AccountManagerService();
        const accountIds = accountManagerService.getAccountManagerIds();
        accountManagerService.synchronizeData(accountIds, function (response) {
            if (response.success) {
                setAccountManagers(response.data);
            }
        });
    }

    return (
        <div className="container">
            <div className="p-5 bg-white rounded shadow mb-5">
                <h1 className="box-header">
                    Excluded account managers
                </h1>
                <div className="row">
                    <div className="col-sm-9">
                        <p>
                            Click on each account manager to allow/deny exporting their invoices
                        </p>
                    </div>
                    <div className="col-sm-3 text-right">
                        <input type="button" className="btn btn-sm btn-light" onClick={() => synchornizeData()} title="Get all lastest account managers" value="Fetch data" />
                    </div>
                </div>
                {renderAccountManagers()}
                <div className="mt-4">
                    <small className="text-muted">If you cannot find the accounts, please use Fetch data button to fetch all the lastest data</small>
                </div>
            </div>
        </div>
    )
}

export default ExcludedAccountManager