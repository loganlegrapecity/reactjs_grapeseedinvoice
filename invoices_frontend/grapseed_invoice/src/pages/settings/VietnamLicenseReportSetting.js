import React, { Component } from 'react';
import SettingAPI from '../../services/SettingAPI';
import { showSuccess } from '../../helper/util';
export default class VietnamLicenseReportSetting extends Component {

    constructor(props) {
        super(props);
        this.onFormChange = this.onFormChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.state = {
            school: "",
            campus: "",
            class: "",
            startRow: "",
            currentUnit: "",
            totalAdjustment: "",
            totalBilled: "",
            littleSeed: "",
            sheetIndex: ""
        };
    }

    setFormValue = (data) => {
        this.setState({
            school: data.school,
            campus: data.campus,
            class: data.class,
            startRow: data.startRow,
            currentUnit: data.currentUnit,
            totalAdjustment: data.totalAdjustment,
            totalBilled: data.totalBilled,
            littleSeed: data.littleSeed,
            sheetIndex: data.sheetIndex
        });
    }

    componentDidMount = () => {
        const that = this;
        new SettingAPI().getVietnamLicenseSetting(function (response) {
            if (response.success) {
                that.setFormValue(response.data);
            }
        });
    }

    onFormChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    resetToDefault() {
        const that = this;
        new SettingAPI().resetVietnamLicenseSetting(function (response) {
            if (response.success) {
                that.setFormValue(response.data);
                showSuccess("Reset successfully!");
            }
        });
    }

    onFormSubmit(e) {
        e.preventDefault();
        let model = {
            school: this.state.school,
            campus: this.state.campus,
            class: this.state.class,
            startRow: this.state.startRow * 1,
            currentUnit: this.state.currentUnit,
            totalAdjustment: this.state.totalAdjustment,
            totalBilled: this.state.totalBilled,
            littleSeed: this.state.littleSeed,
            sheetIndex: this.state.sheetIndex * 1
        };
        new SettingAPI().saveVietnamLicenseSetting(model, function (response) {
            if (response.success) {
                showSuccess('Save successfully');
            }
        });
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="p-5 bg-white rounded shadow mb-5">
                        <h1 className="box-header">
                            Vietnam Region License Report
                        </h1>
                        <form onSubmit={this.onFormSubmit}>
                            <div className="row">
                                <div className="col">
                                    <div className="form-group">
                                        <label>School</label>
                                        <input type="text" maxLength="2" className="form-control" name="school" value={this.state.school} onChange={this.onFormChange} required />
                                    </div>
                                    <div className="form-group">
                                        <label>Campus</label>
                                        <input type="text" maxLength="2" className="form-control" name="campus" value={this.state.campus} onChange={this.onFormChange} required />
                                    </div>
                                    <div className="form-group">
                                        <label>Class</label>
                                        <input type="text" maxLength="2" className="form-control" name="class" value={this.state.class} onChange={this.onFormChange} required />
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Total adjustments</label>
                                        <input type="text" className="form-control" name="totalAdjustment" value={this.state.totalAdjustment} onChange={this.onFormChange} required />
                                    </div>
                                    <div className="form-group">
                                        <label>Start data row index</label>
                                        <input type="number" maxLength="3" className="form-control" name="startRow" value={this.state.startRow} onChange={this.onFormChange} required />
                                    </div>
                                    <div className="form-group">
                                        <label>Curent unit</label>
                                        <input type="text" maxLength="1" className="form-control" name="currentUnit" value={this.state.currentUnit} onChange={this.onFormChange} required />
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="form-group">
                                        <label>Total billed</label>
                                        <input type="text" maxLength="1" className="form-control" name="totalBilled" value={this.state.totalBilled} onChange={this.onFormChange} required />
                                    </div>
                                    <div className="form-group">
                                        <label>LittleSEED License</label>
                                        <input type="text" maxLength="1" className="form-control" name="littleSeed" value={this.state.littleSeed} onChange={this.onFormChange} required />
                                    </div>
                                    <div className="form-group">
                                        <label>Sheet index</label>
                                        <select className="form-control" name="sheetIndex" value={this.state.sheetIndex} onChange={this.onFormChange}>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div className="text-center mt-2">
                                <input type="button" className="btn-grape-white mr-3" onClick={() => this.resetToDefault()} value="Reset to default" />
                                <button type="submit" className="btn-grape btn-long">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}