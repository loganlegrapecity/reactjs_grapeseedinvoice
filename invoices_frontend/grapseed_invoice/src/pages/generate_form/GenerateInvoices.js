import React, { Component, useState, useEffect, useContext } from 'react';
import { GlobalContext } from '../../contexts/GlobalContext';
import DatasourceService from '../../services/DatasourceService';
import GrapeSeedCommonAPI from '../../services/GrapeSeedCommonAPI';
import SchoolMultipleSelect from '../../components/SchoolMultipleSelect';

const GenerateInvoices = (props) => {

    const [schools, setSchools] = useState([]);
    const {generateInvoiceParameters} = useContext(GlobalContext);
    // const toggleShowOption = () => {
    //     this.setState({
    //         showOption: !this.state.showOption
    //     });
    // }

    const handleSubmit = (e) => {
        e.preventDefault();
        new DatasourceService().process(generateInvoiceParameters);
    }
 
    useEffect(() => {
        new GrapeSeedCommonAPI().getSchool(function (response) {
            setSchools(response);
        });
    }, []);

    const generateForm = () => {
        // if (!this.state.showOption) {
        //     return (
        //         <div className="text-center">
        //             <input type="button" className="btn-grape btn-big mt-5" onClick={() => this.toggleShowOption()} value="Start Now" />
        //         </div>
        //     )
        // }

        return (
            <div className="p-5 bg-white rounded shadow mb-5 mt-5">
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <div className="row">
                            <div className="col">
                                <label>Template</label>
                                <select className="form-control">
                                    <option></option>
                                </select>
                            </div>
                            <div className="col">
                                <label>VND to USD</label>
                                <input type="text" className="form-control" />
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <label>Specific schools</label>
                        <SchoolMultipleSelect schools={schools}></SchoolMultipleSelect>
                    </div>
                    <div className="text-center mt-5">
                        <button type="submit" className="btn-grape btn-big">Generate invoices</button>
                    </div>
                </form>
            </div>
        )
    }

    return (
        <div className="row mt-2">
            <div className="col-sm-12 mt-1" id="m-generate-invoice">
                <h1 id="big-quote" className="text-center">GrapeSEED Invoice</h1>
                <p id="small-quote" className="text-center">It's very simple to create your invoices, click the button below and take a cup of coffee</p>
                {generateForm()}
            </div>
        </div>
    )
}

export default GenerateInvoices