import React, { Component, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import AuthenticationService from '../../services/AuthenticationService';

const Login = () => {

    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        new AuthenticationService().signIn(email, password);
    }

    useEffect(() => {

    }, []); // [] is indicator for componentDidmount ??

    return (
        <div className="container">
            <div className="row">
                <div className="col-sm-12 col-md-4 offset-md-4">
                    <form id="login-form" onSubmit={handleSubmit}>
                        <h2 className="text-center">Welcome</h2>
                        <div id="logo-img"></div>
                        <input type="email"
                            placeholder="Email"
                            required
                            value={email} onChange={(e) => setEmail(e.target.value)}
                            className="login-form-control" />

                        <input type="password"
                            placeholder="Password"
                            required
                            value={password} onChange={(e) => setPassword(e.target.value)}
                            className="login-form-control" />
                        <small className="text-primary text-center d-block">
                            Login using your account from http://schools.grapeseed.com
                        </small>
                        <div className="text-center">
                            <input type="submit" className="btn-grape mt-4" value="Login" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Login;