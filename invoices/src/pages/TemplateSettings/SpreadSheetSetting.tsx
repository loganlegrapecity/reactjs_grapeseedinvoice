import React, { Component } from 'react';
import TemplateSetting from '../../components/excel-settings/TemplateSetting';
export default class SpreadSheetSetting extends React.Component {
    render() {
        return (
            <div>
                <div className="container">
                    <div className="p-5 bg-white rounded shadow mb-5">
                        {/* Lined tabs*/}
                        <ul id="myTab2" role="tablist" className="nav nav-tabs nav-pills with-arrow lined flex-column flex-sm-row text-center">
                            <li className="nav-item flex-sm-fill">
                                <a id="home2-tab" data-toggle="tab"
                                    href="#home2" role="tab" aria-controls="home2" aria-selected="true"
                                    className="nav-link text-uppercase font-weight-bold mr-sm-3 rounded-0 active">Invoice template</a>
                            </li>
                            <li className="nav-item flex-sm-fill">
                                <a id="profile2-tab" data-toggle="tab"
                                    href="#profile2" role="tab" aria-controls="profile2"
                                    aria-selected="false" className="nav-link text-uppercase font-weight-bold mr-sm-3 rounded-0">
                                        Vietnam region license report
                                    </a>
                            </li> 
                        </ul>
                        <div id="myTab2Content" className="tab-content">
                            <div id="home2" role="tabpanel" aria-labelledby="home-tab" className="tab-pane fade py-5 show active">
                                <TemplateSetting></TemplateSetting>
                            </div>
                            <div id="profile2" role="tabpanel" aria-labelledby="profile-tab" className="tab-pane fade py-5">
                                
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
