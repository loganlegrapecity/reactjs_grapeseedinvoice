import React, { Component } from 'react';
import InvoiceSetting from '../../components/settings/InvoiceSetting';
import ExcludeAccountManagers from '../../components/settings/ExcludeAccountManagers';

export default class Setting extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="p-5 bg-white rounded shadow mb-5">
                    {/* Lined tabs*/}
                    <ul id="myTab2" role="tablist" className="nav nav-tabs nav-pills with-arrow lined flex-column flex-sm-row text-center">
                        <li className="nav-item flex-sm-fill">
                            <a id="home2-tab" data-toggle="tab" 
                               href="#home2" role="tab" aria-controls="home2" aria-selected="true" 
                               className="nav-link text-uppercase font-weight-bold mr-sm-3 rounded-0 active">Invoice settings</a>
                        </li>
                        <li className="nav-item flex-sm-fill">
                            <a id="profile2-tab" data-toggle="tab" 
                               href="#profile2" role="tab" aria-controls="profile2" 
                               aria-selected="false" className="nav-link text-uppercase font-weight-bold mr-sm-3 rounded-0">Excluded Account managers</a>
                        </li>
                        {/* <li className="nav-item flex-sm-fill">
                            <a id="contact2-tab" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact2" aria-selected="false" className="nav-link text-uppercase font-weight-bold rounded-0">Contact</a>
                        </li> */}
                    </ul>
                    <div id="myTab2Content" className="tab-content">
                        <div id="home2" role="tabpanel" aria-labelledby="home-tab" className="tab-pane fade py-5 show active">
                           <InvoiceSetting/>
                        </div>
                        <div id="profile2" role="tabpanel" aria-labelledby="profile-tab" className="tab-pane fade py-5">
                            <ExcludeAccountManagers></ExcludeAccountManagers>
                        </div>
                        {/* <div id="contact2" role="tabpanel" aria-labelledby="contact-tab" className="tab-pane fade px-4 py-5">
                            33
                        </div> */}
                    </div> 
                </div>
            </div>
        );
    }
}
