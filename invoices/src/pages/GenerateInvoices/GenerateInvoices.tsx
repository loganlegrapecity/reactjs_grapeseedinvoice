import React, { Component } from 'react';
import { GlobalContext } from '../../contexts/GlobalContext';
import DatasourceService from '../../services/DatasourceService';
import GrapeSeedCommonAPI from '../../services/GrapeSeedCommonAPI';

interface GenerateInvoicesState {
    showOption: boolean,
    schools: []
}

export default class GenerateInvoices extends React.Component<{}, GenerateInvoicesState> {

    constructor(props: any) {
        super(props);
        this.state = {
            showOption: false,
            schools: []
        }
    }

    static contextType = GlobalContext;

    toggleShowOption = () => {
        this.setState({
            showOption: !this.state.showOption
        });
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        new DatasourceService().process(this.state.schools);
    }

    handleSelectChange = (): void => {

    }

    componentDidMount() {
        const that = this;
        new GrapeSeedCommonAPI().getSchool(function (response: any) {
            that.setState({
                schools: response//.data
            });
        });
    }

    generateForm = () => {
        // if (!this.state.showOption) {
        //     return (
        //         <div className="text-center">
        //             <input type="button" className="btn btn-grape btn-big mt-5" onClick={() => this.toggleShowOption()} value="Start Now" />
        //         </div>
        //     )
        // }

        return (
            <div className="p-5 bg-white rounded shadow mb-5 mt-5">
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <div className="row">
                            <div className="col">
                                <label>Template</label>
                                <select className="form-control">
                                    <option></option>
                                </select>
                            </div>
                            <div className="col">
                                <label>VND to USD</label>
                                <input type="text" className="form-control" />
                            </div>
                        </div>
                        {/* <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small> */}
                    </div>

                    <div className="form-group">
                        <label>Specific schools</label>
                        <select className="form-control">
                            <option></option>
                        </select>
                    </div>
                    <div className="text-center mt-5">
                        <button type="submit" className="btn btn-grape btn-big">Generate invoices</button>
                    </div>
                </form>
            </div>
        )
        // const {toggleLoading} = this.context;
        //  toggleLoading();
        // new GrapeSeedCommonAPI().getSchool();
    }

    render() {
        return (
            <div className="row mt-2">
                <div className="col-sm-12 mt-1" id="m-generate-invoice">
                    <h1 id="big-quote" className="text-center">GrapeSEED Invoice</h1>
                    <p id="small-quote" className="text-center">It's very simple to create your invoices, click the button below and take a cup of coffee</p>
                    {this.generateForm()}
                </div>
            </div>
        );
    }
}
