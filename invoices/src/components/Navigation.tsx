import React from 'react';
import { NavLink } from "react-router-dom";
import AuthenticationService from '../services/AuthenticationService';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

const Navigation = () => {
    const signout = () => {

        confirmAlert({
            title: 'Confirm to sign out',
            message: 'Are you sure?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {
                        new AuthenticationService().signOut();
                        window.location.reload();
                    }
                },
                {
                    label: 'No',
                    onClick: () => {}
                }
            ]
        });
    }

    return (
        <div id="top-navigation" className="clear">
            <div id="logo">
                <NavLink exact to="/generate-invoices">Logo</NavLink>
            </div>
            <nav>
                <NavLink to="/generate-invoices">Generate invoices</NavLink>
                <NavLink to="/setting">Setting</NavLink>
                <NavLink to="/spreadsheet-setting">Excel setting</NavLink>
                <a onClick={() => signout()}>SIGN OUT</a>
            </nav>
        </div>
    )
}

export default Navigation