import React from 'react';

const ExcludeAccountManagers = () => {
    return (
        <form>
            <div className="form-group">
                <label>
                    Enter the email or name of the account managers that you want to ignore for generating the invoices
                </label>
                <span className="text-muted d-block">
                    Emails or names are separated by comma(,) or semicolon(,)
                </span>
                <small className="text-danger">
                    zGSVN admin is always ignored even you set or not
                </small>
                <textarea className="form-control mt-2" rows={4}></textarea>
            </div>
            <div className="row mt-4">
                <div className="col-sm-12 col-md-4 offset-md-4">
                    <button type="submit" className="btn btn-grape btn-block">Save</button>
                </div>
            </div>
        </form>
    )
}

export default ExcludeAccountManagers;