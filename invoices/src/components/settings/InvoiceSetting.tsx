import React from 'react';

const InvoiceSetting = () => {
    return (
        <form>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">LittleSEED license price/year</label>
                <input type="number" className="form-control" placeholder="Enter price" />
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">GrapeSEED license price/year</label>
                <input type="number" className="form-control" placeholder="Enter price" />
            </div>
            <div className="form-check">
                <input type="checkbox" className="form-check-input" id="chk-exclude-material-products" />
                <label className="form-check-label" htmlFor="chk-exclude-material-products">
                    Exclude material products from invoices
                </label>
            </div>
            <div className="row mt-4">
                <div className="col-sm-12 col-md-4 offset-md-4">
                    <button type="submit" className="btn btn-grape btn-block">Save</button>
                </div>
            </div>
        </form>
    )
}

export default InvoiceSetting;