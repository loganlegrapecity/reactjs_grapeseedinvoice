import * as React from 'react'
import './styles/App.scss';
import Setting from './pages/Settings/Setting';
import GenerateInvoices from './pages/GenerateInvoices/GenerateInvoices';
import SpreadSheetSetting from './pages/TemplateSettings/SpreadSheetSetting';
import GlobalContextProvider, { GlobalContext } from './contexts/GlobalContext';

import {
  BrowserRouter,
  Switch,
  withRouter,
  Route,
  Link,
  NavLink,
  useParams
} from "react-router-dom";
import Navigation from './components/Navigation';
import Login from './pages/Login/Login';
import { changeBodyClass, isValidToken } from './helper/util';


function App() {

  isValidToken();

  if (!isValidToken()) {
    changeBodyClass('login-page');
    return (
      <BrowserRouter>
        <Login></Login>
      </BrowserRouter>
    )
  }

  return (
    <GlobalContextProvider>
      <div className="container">
        <BrowserRouter>
          <Navigation></Navigation>
          <div id="main-body">
            <Switch>
              <Route exact path="/login">
                <Login></Login>
              </Route>
              <Route exact path="/">
                <GenerateInvoices></GenerateInvoices>
              </Route>
              <Route exact path="/generate-invoices">
                <GenerateInvoices></GenerateInvoices>
              </Route>
              <Route exact path="/setting">
                <Setting></Setting>
              </Route>
              <Route exact path="/spreadsheet-setting">
                <SpreadSheetSetting />
              </Route>
            </Switch>
          </div>
        </BrowserRouter>
      </div>
    </GlobalContextProvider>
  );
}

export default App;
