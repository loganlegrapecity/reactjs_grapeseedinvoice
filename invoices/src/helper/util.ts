import $ from 'jquery';

export function loadingMask() {
    if ($(".loading-mask").length) {
        $(".loading-mask").remove();
    } else {
        $("body").append("<div class='loading-mask'></div>")
    }
}

export function changeBodyClass(className: string) {
    $("body").addClass(className);
}

export function parseJwt(token: string) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
};

export function isValidToken(): boolean {
    const token = localStorage.getItem("token");
    if (!token)
        return false;

    const obj = parseJwt(token);
    const expireDate = obj.exp;
    return new Date(expireDate * 1000) > new Date();
}