import axios from 'axios';
import { loadingMask } from './util';
import { RootApi } from '../consts/ApiConst';

export function apiPost(apiUrl: string, model: any, callbackFunction: any, turnOffLoading: boolean = false) {
    if (!turnOffLoading) {
        loadingMask();
    }

    axios({
        method: 'post',
        url: standardApi(apiUrl),
        data: model,
        headers: {
            'Auth': localStorage.getItem("token")
        }
    }).then(function (response) {
        if (!turnOffLoading) {
            loadingMask();
        }
        if (response.data.success) {
            if (callbackFunction)
                callbackFunction(response.data);
        } else {
            // error
        }
    });
}

export function apiGet(apiUrl: string, callbackFunction: any, turnOffLoading: boolean = false) {
    if (!turnOffLoading) {
        loadingMask();
    }

    axios({
        method: 'get',
        url: standardApi(apiUrl),
        headers: {
            'Auth': localStorage.getItem("token")
        }
    }).then(function (response) {
        if (!turnOffLoading) {
            loadingMask();
        }
        if (response.data.success) {
            if (callbackFunction)
                callbackFunction(response.data);
        } else {
            // error
        }
    });
}

export function standardApi(api: string): string {
    if (api.startsWith(RootApi)) {
        return api;
    }

    if (!RootApi.endsWith("/") && !api.startsWith("/")) {
        return RootApi + "/" + api;
    }

    return RootApi + api;
}