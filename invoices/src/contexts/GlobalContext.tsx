import React, { Component } from 'react';

export interface GlobalContextInterface {
    
}

export const GlobalContext = React.createContext<GlobalContextInterface | null>(null);

export default class GlobalContextProvider extends Component {

    state = {
       
    };

    // toggleLoading = () => {
    //     this.setState({
    //         loadingMask: !this.state.loadingMask
    //     })
    // }

    render() {
        return (
            <GlobalContext.Provider value={{ ...this.state }}>
                {this.props.children}
            </GlobalContext.Provider>
        );
    }
}
