import { GlobalContextInterface } from './../contexts/GlobalContext';

const toggleBoolean = (contextValue: GlobalContextInterface, state: boolean): GlobalContextInterface => {
    return { ...contextValue, loadingMask: state };

    // contextValue.loadingMask = state;
    // return contextValue;
}

export const GlobalReducer = (state: GlobalContextInterface, action: any): GlobalContextInterface => {
    switch (action.type) {
        case 'TOGGLE':
            return toggleBoolean(state, action.payload);
        default:
            return state;
    }
}