export interface SchoolApiItemResponse{
    id:string,
    name:string,
    gsNumber:string,
    trainerName:string,
    campusName:string
}

export interface SchoolResponse{
    schools:Array<SchoolApiItemResponse>
}