import { School } from "./School";

export interface AccountManager{
    id: string,
    name:string,
    emai:string,
    schools: Array<School>
}