// export enum ApiConst{
//     TokenApi = "https://account.grapeseed.com/connect/token",
//     AdminApi = "https://services.grapeseed.com/account/v1/users",
//     SchoolApi = 'https://services.grapeseed.com/admin/v1/schools?offset=0&disabled=false&regionId=49c384f1-8f63-40f4-8ff1-3e57d139c3d5&sortBy=name&isDescending=false&1584007448469',
//     AccountManagersOfSchoolApi = "https://services.grapeseed.com/admin/v1/schools/{0}/accountmanagers?",
//     SchoolAddressApi = "https://services.grapeseed.com/admin/v1/schools/{0}/campuses?offset=0&limit=10&disabled=false&sortBy=name&isDescending=false",
//     MaterialProducts ="https://services.grapeseed.com/admin/v1/regions/49c384f1-8f63-40f4-8ff1-3e57d139c3d5/materialorders?status=3",
//     Products = "https://services.grapeseed.com/admin/v1/products/local?offset=0&limit=2000&disabled=false&filterText=vietnam&sortBy=regionEnglishName&isDescending=false&1584346635478",
//     ProductPrice = "https://services.grapeseed.com/admin/v1/products/local/{0}",
//     CampusGsNumber = "https://services.grapeseed.com/admin/v1/schools/{0}/campuses?offset=0&limit=100&disabled=false"
// }

export const RootApi = "https://localhost:44303";
//Proxy URL
export enum ApiConst{
    LoginApi = "/api/authentication/sign-in",
    AdminApi = "/api/grapeseed/accounts",
    SchoolApi = '/api/grapeseed/schools',
    MaterialProducts ="/api/grapeseed/material-products",
    Products = "/api/grapeseed/products",
    ProductPrice = "https://services.grapeseed.com/admin/v1/products/local/{0}",
    CampusGsNumber = "https://services.grapeseed.com/admin/v1/schools/{0}/campuses?offset=0&limit=100&disabled=false"
}


