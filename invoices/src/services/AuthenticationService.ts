import { ApiConst } from "../consts/ApiConst";
import { standardApi } from "../helper/apiHelper";
import axios from 'axios';

export default class AuthenticationService {
    signIn(email: string, password: string) {

        axios.post(standardApi(ApiConst.LoginApi), { email, password })
            .then(function (response: any) {
                // login success
                if (response.data.access_token && response.data.access_token.length) {
                    localStorage.setItem("token", response.data.access_token);
                    window.location.reload();
                } else {
                    // login fail
                    alert('Login failure');
                }
            })
    }

    signOut() {
        localStorage.removeItem("token");
        localStorage.removeItem("schools");
        window.location.reload();
    }
}