import { ApiConst, RootApi } from "../consts/ApiConst";
import AuthenticationService from "./AuthenticationService";
import { apiGet } from "../helper/apiHelper";
import { isValidToken } from "../helper/util";
export default class GrapeSeedCommonAPI {

    constructor() {
        if (!isValidToken()) {
            new AuthenticationService().signOut();
        }
    }

    getSchool = (callback: any) => {
        const schoolsData = localStorage.getItem("schools");
        if (schoolsData?.length) {  
            callback(JSON.parse(schoolsData).schools);
            return;
        }

        apiGet(ApiConst.SchoolApi, function (response: any) {
            if (response.data?.schools?.length) {
                localStorage.setItem("schools", JSON.stringify(response.data));
            }

            if (!callback) {
                callback(response.data.schools)
            }
        });
    }

    getAccountManagers = (accountIds: string[], callback: any) => {
        if (accountIds?.length == 0)
            return;

        const ids = accountIds.join();
        apiGet(ApiConst.AdminApi + "?accountIds=" + ids, callback, true);
    }

    getMaterialProducts(callback: any) {
        apiGet(ApiConst.MaterialProducts, callback, true);
    }

    getProducts(callBack: any) {
        apiGet(ApiConst.Products, callBack, true);
    }

    getProductDetail(productId: string, callBack: any) {
        apiGet(ApiConst.ProductPrice.replace("{0}", productId), callBack, true);
    }

    getGsNumber(schoolId: string, callback: any) {
        apiGet(ApiConst.CampusGsNumber.replace("{0}", schoolId), callback, true);
    }
}