import GrapeSeedCommonAPI from "./GrapeSeedCommonAPI"

interface AccountManager {
    id: string,
    name: string,
    email: string
}

export default class DatasourceService {
    process = (schools: []) => {
        const accountIds: string[] = [];
        const accountManagers: AccountManager[] = [];
        let orderedMaterialProducts: any[] = [];
        let productsListInDatabase = [];

        const apiCaller = new GrapeSeedCommonAPI();

        // get all accountIds
        schools.map((s: any) => {
            if (s && s.accountManagerIds && s.accountManagerIds.length) {
                s.accountManagerIds.map((id: string) => {
                    if (accountIds.indexOf(id) < 0) {
                        accountIds.push(id);
                    }
                });
            }
        });

        // get accounts
        apiCaller.getAccountManagers(accountIds, function (response: any) {
            const accounts = response.data;
            if (accounts && accounts.length) {
                accounts.map((a: any) => {
                    accountManagers.push({
                        id: a.id,
                        name: a.name,
                        email: a.email
                    })
                });
            }
        });

        //get material products
        apiCaller.getMaterialProducts(function (response: any) {
            orderedMaterialProducts = response.data;
            if (orderedMaterialProducts?.length) {
                apiCaller.getProducts(function (products: any) {
                    productsListInDatabase = products.data;
                });
            }
        });
    }
}