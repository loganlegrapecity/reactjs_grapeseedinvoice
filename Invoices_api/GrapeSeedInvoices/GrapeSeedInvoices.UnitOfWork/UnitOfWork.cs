﻿using System;
using System.Threading.Tasks;
using GrapeSeedInvoices.DbModels;
using GrapeSeedInvoices.DbModels.Entities;
using GrapeSeedInvoices.UnitOfWork.Repository.Common;

namespace GrapeSeedInvoices.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IGenericRepository<Option> optionRepository;
        private IGenericRepository<AccountManager> accountManagerRepository; 

        public UnitOfWork(IDbContext context) // : base(context)
        {
            this.DbContext = context;
        }

        public IGenericRepository<Option> OptionRepository
        {
            get
            {
                if(optionRepository==null)
                    optionRepository = new GenericRepository<Option>(DbContext);
                
                return optionRepository;
            }
        }

        public IGenericRepository<AccountManager> AccountManagerRepository
        {
            get
            {
                if(accountManagerRepository==null)
                    accountManagerRepository = new GenericRepository<AccountManager>(DbContext);

                return accountManagerRepository;
            }
        }

        #region UnitOfWork methods

        /// <summary>
        /// true means dbContext was disposed
        /// </summary>
        protected bool Disposed;

        /// <summary>
        /// The DbContext
        /// </summary>
        protected readonly IDbContext DbContext;

        ~UnitOfWork()
        {
            this.Dispose(false);
        }

        /// <inheritdoc />
        public async Task<int> CommitAsync()
        {
            // Save changes with the default options
            return await System.Threading.Tasks.Task.FromResult(this.DbContext.SaveChanges());
        } 

        /// <inheritdoc />
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(true);
        }

        /// <summary>
        /// Disposes all external resources.
        /// </summary>
        /// <param name="disposing">The dispose indicator.</param>
        private void Dispose(bool disposing)
        {
            if (this.Disposed)
            {
                return;
            }

            this.DbContext.Dispose();
            this.Disposed = true;

            if (!disposing)
            {
                return;
            }
        }

        #endregion
    }
}
