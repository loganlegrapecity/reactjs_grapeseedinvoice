﻿using GrapeSeedInvoices.DbModels.Entities;
using GrapeSeedInvoices.UnitOfWork.Repository.Common;
using System;
using System.Threading.Tasks;

namespace GrapeSeedInvoices.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> CommitAsync();

        IGenericRepository<Option> OptionRepository { get; }

        IGenericRepository<AccountManager> AccountManagerRepository { get; }
    }
}
