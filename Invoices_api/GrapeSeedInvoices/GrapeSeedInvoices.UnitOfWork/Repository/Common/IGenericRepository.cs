﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace GrapeSeedInvoices.UnitOfWork.Repository.Common
{
    public interface IGenericRepository<T>
    {
        /// <summary>
        /// Get all models
        /// </summary>
        /// <returns>IQueryable<T></returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Get by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetById<KeyDataType>(KeyDataType id);

        /// <summary>
        /// IEnumerable
        /// </summary>
        /// <param name="predicate">Predicate</param>
        /// <returns>T</returns>
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="entity">entity</param>
        /// <returns>T</returns>
        Task<T> Add(T entity);

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns>T</returns>
        T Delete(T entity);

        /// <summary>
        /// Delete by range
        /// </summary>
        /// <param name="entities"></param>
        void DeleteRange(IEnumerable<T> entities);

        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="entity">Entity</param>
        /// <returns>Task</returns>
        void Edit(T entity);
    }
}
