﻿using GrapeSeedInvoices.DbModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapeSeedInvoices.UnitOfWork.Repository.Common
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        /// <summary>
        /// DbContext
        /// </summary>
        protected IDbContext Entities;

        /// <summary>
        /// Dbset
        /// </summary>
        protected readonly DbSet<T> Dbset;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// </summary>
        /// <param name="context">IDbcontext</param>
        public GenericRepository(IDbContext context)
        {
            this.Entities = context;
            this.Dbset = context.Set<T>();
        }

        /// <inheritdoc />
        public virtual IQueryable<T> GetAll()
        {
            return this.Dbset;
        }

        /// <inheritdoc />
        public virtual async Task<T> GetById<KeyDataType>(KeyDataType id)
        {
            return await this.Dbset.FindAsync(id);
        }

        /// <inheritdoc />
        public virtual IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            var query = this.Dbset.Where(predicate);
            return query;
        }

        /// <inheritdoc />
        public virtual async Task<T> Add(T entity)
        {
            return (await this.Dbset.AddAsync(entity)).Entity;
        }

        /// <inheritdoc />
        public virtual T Delete(T entity)
        {
            this.Dbset.Attach(entity);
            return this.Dbset.Remove(entity).Entity;
        }


        /// <inheritdoc />
        public virtual void DeleteRange(IEnumerable<T> entities)
        {
            if (entities != null)
                this.Dbset.RemoveRange(entities);
        }

        /// <inheritdoc />
        public virtual void Edit(T entity)
        {
            if (this.Entities != null)
            {
                this.Entities.Entry(entity).State = EntityState.Modified;
            }
        }
    }
}
