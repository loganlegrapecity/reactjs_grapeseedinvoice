﻿using GrapeSeedInvoices.CommonAPI.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using GrapeSeedInvoices.ApiModels;

namespace GrapeSeedInvoices.CommonAPI
{
    public class AuthenticationApiService : IAuthenticationApiService
    {
        public async Task<TokenResponse> Login(LoginApiModel model)
        {
            using (var client = new HttpClient())
            {
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("scope", "offline_access basicinfo openid"),
                    //new KeyValuePair<string, string>("username","malone.dunlavy@grapeseed.com"),
                    //new KeyValuePair<string, string>("password","Ma10n3.."),

                    new KeyValuePair<string, string>("username",model.Email),
                    new KeyValuePair<string, string>("password",model.Password),
                });


                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Add("Content", "application/x-www-form-urlencoded");

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic Z3JhcGVzZWVkLmdlbmVyYWw6RjAwOEM5OUY1MDFDNDg3M0E4RDczNjIxQUI4OEYxMEU=");
                var res = client.PostAsync(ApiConst.TokenApi, formContent).Result;

                if (res == null || !res.IsSuccessStatusCode)
                    return null;

                var response = await res.Content.ReadAsStringAsync();
                var token = JsonConvert.DeserializeObject<TokenResponse>(response);
                return token;

            }
        }
    }
}
