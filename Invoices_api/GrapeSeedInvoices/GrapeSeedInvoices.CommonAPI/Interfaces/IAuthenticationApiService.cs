﻿using System.Threading.Tasks;
using GrapeSeedInvoices.ApiModels;

namespace GrapeSeedInvoices.CommonAPI.Interfaces
{
    public interface IAuthenticationApiService
    {
        Task<TokenResponse> Login(LoginApiModel model);
    }
}
