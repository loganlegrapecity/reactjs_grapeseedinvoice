﻿using System.Collections.Generic;
using GrapeSeedInvoices.ApiModels;

namespace GrapeSeedInvoices.CommonAPI.Interfaces
{
    public interface IGrapeSeedApiService
    {
        string Token { get; set; }

        bool IsTestEnvironment { get; set; }

        SchoolListApiModel GetSchools();

        List<AccountApiModel> GetAccounts(string schoolId);

        List<AccountApiModel> GetAccounts(List<string> accountIds);

        List<string> GetAccountIdsOfSchool(string schoolId);

        string GetSchoolAddress(string schoolId);

        List<MaterialProductApiModel> GetMaterialProducts();

        List<ProductApiModel> GetProducts();

        ProductDetailApiModel GetProductDetail(string productId);

        List<CampusGsNumberApiModel> GetGsNumber(string schoolId);
    }
}
