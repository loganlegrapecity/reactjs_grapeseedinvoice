﻿using GrapeSeedInvoices.CommonAPI.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using GrapeSeedInvoices.ApiModels;

namespace GrapeSeedInvoices.CommonAPI
{
    public class AccessToken
    {
        public string Token { get; set; }

        public DateTime ExpireDate { get; set; }
    }

    /// <summary>
    /// Class is used to fetch all data from GrapeSeed api
    /// </summary>
    public class GrapeSeedApiService : IGrapeSeedApiService
    {
        public bool IsTestEnvironment { get; set; }

        public string Token { get; set; }

        public List<AccountApiModel> GetAccounts(string schoolId)
        {
            var accountIds = GetAccountIdsOfSchool(schoolId);
            if (accountIds == null || accountIds?.Count == 0)
                return null;

            return GetAccounts(accountIds);
        }

        public List<AccountApiModel> GetAccounts(List<string> accountIds)
        {
            var api = ApiConst.AdminApi;
            var ids = accountIds.Aggregate("", (current, id) => current + $"ids={id}&");

            api += "?" + ids;
            return GetApi<List<AccountApiModel>>(api);
        }

        public SchoolListApiModel GetSchools()
        {
            var schools = GetApi<SchoolListApiModel>(ApiConst.SchoolApi);
            return schools;
        }

        public List<string> GetAccountIdsOfSchool(string schoolId)
        {
            var api = string.Format(ApiConst.AccountManagersOfSchoolApi, schoolId);
            return GetApi<List<string>>(api);
        }

        public string GetSchoolAddress(string schoolId)
        {
            var addresses = GetApi<List<SchoolAddressApiModel>>(string.Format(ApiConst.SchoolAddressApi, schoolId));
            if (addresses == null || addresses.Count == 0)
            {
                return null;
            }

            var primary = addresses.FirstOrDefault(x => x.isPrimary &&
                                                                                            (!string.IsNullOrWhiteSpace(x.address1) ||
                                                                                             !string.IsNullOrWhiteSpace(x.address1)));
            if (primary != null)
                return primary.address1 ?? primary.address2;

            var firstOne = addresses.FirstOrDefault(x =>
                !string.IsNullOrWhiteSpace(x.address1) || !string.IsNullOrWhiteSpace(x.address2));

            return firstOne?.address1 ?? firstOne?.address2;
        }

        public List<MaterialProductApiModel> GetMaterialProducts()
        {
            var excludeMaterialProducts = false;
            //TODO: check exclude material products or not
            if(excludeMaterialProducts)
                return new List<MaterialProductApiModel>();

            var data = GetApi<List<MaterialProductApiModel>>(ApiConst.MaterialProducts);
            return data?.Where(x => x.submissionDate >= DateTime.Now.AddDays(-45).Date)?.ToList();
        }

        public List<ProductApiModel> GetProducts()
        {
            return GetApi<List<ProductApiModel>>(ApiConst.Products);
        }

        public ProductDetailApiModel GetProductDetail(string productId)
        {
            return GetApi<ProductDetailApiModel>(string.Format(ApiConst.ProductPrice, productId));
        }

        public List<CampusGsNumberApiModel> GetGsNumber(string schoolId)
        {
            return GetApi<List<CampusGsNumberApiModel>>(string.Format(ApiConst.CampusGsNumber, schoolId));
        }

        #region Helper
        private AccessToken AccessToken { get; set; }

        private string GetToken()
        {
            if (!string.IsNullOrWhiteSpace(AccessToken?.Token) && AccessToken?.ExpireDate > DateTime.Now.AddMinutes(1))
            {
                return AccessToken.Token;
            }

            using (var client = new HttpClient())
            {
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("scope", "offline_access basicinfo openid"),
                    new KeyValuePair<string, string>("username","malone.dunlavy@grapeseed.com"),
                    new KeyValuePair<string, string>("password","Ma10n3.."),
                });


                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Add("Content", "application/x-www-form-urlencoded");

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Basic Z3JhcGVzZWVkLmdlbmVyYWw6RjAwOEM5OUY1MDFDNDg3M0E4RDczNjIxQUI4OEYxMEU=");
                var res = client.PostAsync(ApiConst.TokenApi, formContent).Result;

                if (res != null && res.IsSuccessStatusCode)
                {
                    var response = res.Content.ReadAsStringAsync().Result;
                    var token = JsonConvert.DeserializeObject<TokenResponse>(response);

                    this.AccessToken = new AccessToken()
                    {
                        Token = token.access_token,
                        ExpireDate = DateTime.Now.AddSeconds(token.expires_in)
                    };

                    return token?.access_token;
                }

                return null;
            }
        }

        private string FormatApi(string api)
        {
            if (IsTestEnvironment)
                return api.Replace("https://services.", "http://test-api.");

            return api;
        }

        /// <summary>
        /// Get data from API
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlApi"></param>
        /// <param name="saveResponseDataFilePath"></param>
        /// <returns></returns>
        private T GetApi<T>(string urlApi, string saveResponseDataFilePath = null)
        {
            var token = this.Token;// GetToken();

            urlApi = FormatApi(urlApi);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Add("Content", "application/x-www-form-urlencoded");

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
                var res = client.GetAsync(urlApi).Result;

                if (res != null && res.IsSuccessStatusCode)
                {
                    var response = res.Content.ReadAsStringAsync().Result;

                    if (!string.IsNullOrWhiteSpace(saveResponseDataFilePath))
                    {
                        System.IO.File.WriteAllText(saveResponseDataFilePath, response);
                    }

                    return JsonConvert.DeserializeObject<T>(response);
                }
            }

            return default(T);
        }

        #endregion 
    }
}
