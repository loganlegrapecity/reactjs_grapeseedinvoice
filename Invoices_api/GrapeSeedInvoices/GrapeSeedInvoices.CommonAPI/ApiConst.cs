﻿namespace GrapeSeedInvoices.CommonAPI
{
    //TODO: move to appsettings.json
    public class ApiConst
    {
        public const string TokenApi = "https://account.grapeseed.com/connect/token";
        public const string SchoolApi = "https://services.grapeseed.com/admin/v1/schools?offset=0&disabled=false&regionId=49c384f1-8f63-40f4-8ff1-3e57d139c3d5&sortBy=name&isDescending=false&1584007448469";
        public const string AdminApi = "https://services.grapeseed.com/account/v1/users";

        public const string AccountManagersOfSchoolApi = "https://services.grapeseed.com/admin/v1/schools/{0}/accountmanagers?";

        public const string SchoolAddressApi = "https://services.grapeseed.com/admin/v1/schools/{0}/campuses?offset=0&limit=10&disabled=false&sortBy=name&isDescending=false";

        public const string MaterialProducts =
            "https://services.grapeseed.com/admin/v1/regions/49c384f1-8f63-40f4-8ff1-3e57d139c3d5/materialorders?status=3";

        public const string Products = "https://services.grapeseed.com/admin/v1/products/local?offset=0&limit=2000&disabled=false&filterText=vietnam&sortBy=regionEnglishName&isDescending=false&1584346635478";
        public const string ProductPrice = "https://services.grapeseed.com/admin/v1/products/local/{0}";

        public const string CampusGsNumber = "https://services.grapeseed.com/admin/v1/schools/{0}/campuses?offset=0&limit=100&disabled=false";
    }
}
