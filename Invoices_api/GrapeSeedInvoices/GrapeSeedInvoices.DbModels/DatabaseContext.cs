﻿using GrapeSeedInvoices.DbModels.Entities;
using Microsoft.EntityFrameworkCore;

namespace GrapeSeedInvoices.DbModels
{
    public class DatabaseContext : Microsoft.EntityFrameworkCore.DbContext, IDbContext
    {
        #region DbSet

        public virtual DbSet<AccountManager> AccountManagers { get; set; }

        public virtual DbSet<Option> Options { get; set; } 

        #endregion

        #region Setting

        private readonly string connectionString;

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }

        public DatabaseContext(string cnn)
        {
            connectionString = cnn;
        }

        /// <summary>
        /// On Configuring event method
        /// </summary>
        /// <param name="optionsBuilder">Option Builder Obect</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public void MarkAsModified(object o, string propertyName)
        {
            this.Entry(o).Property(propertyName).IsModified = true;
        }

        #endregion
    }
}
