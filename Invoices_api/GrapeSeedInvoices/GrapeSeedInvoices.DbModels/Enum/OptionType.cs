﻿namespace GrapeSeedInvoices.DbModels.Enum
{
    public enum OptionType
    {
        InvoiceSetting = 1,

        InvoiceTemplateFilePath = 2,

        InvoiceTemplateCellConfiguration = 3,

        VietNamRegionLicenseReport = 4,

        PriceSetting = 5
    }
}
