﻿using System.ComponentModel.DataAnnotations;

namespace GrapeSeedInvoices.DbModels.Entities
{
    public class AccountManager
    {
        [Key]
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public bool IsIncludedInReport { get; set; }
    }
}
