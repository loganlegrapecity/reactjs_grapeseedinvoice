﻿using System;
using GrapeSeedInvoices.DbModels.Enum;

namespace GrapeSeedInvoices.DbModels.Entities
{
    public class Option
    {
        public string Id { get; set; }

        public OptionType OptionType { get; set; }

        public string JsonValue { get; set; }

        public string LastUpdateBy { get; set; }

        public DateTime? LastUpdate { get; set; }
    }
}
