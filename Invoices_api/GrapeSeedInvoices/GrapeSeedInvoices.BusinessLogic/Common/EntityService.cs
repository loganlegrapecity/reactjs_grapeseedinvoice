﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GrapeSeedInvoices.UnitOfWork;
using GrapeSeedInvoices.UnitOfWork.Repository.Common;
using Microsoft.EntityFrameworkCore;

namespace GrapeSeedInvoices.BusinessLogic.Common
{
    public abstract class EntityService<T> : IEntityService<T>
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IGenericRepository<T> repository;


        protected EntityService(IUnitOfWork unitOfWork, IGenericRepository<T> repository)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
        }

        /// <inheritdoc />
        public virtual async Task<T> GetById<KeyDataType>(KeyDataType id)
        {
            return await this.repository.GetById(id);
        }

        /// <inheritdoc />
        public virtual async Task<T> Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var result = await this.repository.Add(entity);
            return (await this.unitOfWork.CommitAsync()) > 0 ? result : default(T);
        }

        /// <inheritdoc />
        public virtual async Task<bool> Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            this.repository.Edit(entity);
            return (await this.unitOfWork.CommitAsync()) > 0;
        }

        /// <inheritdoc />
        public virtual async Task<bool> Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (this.repository.Delete(entity) != null)
            {
                return await this.unitOfWork.CommitAsync() > 0;
            }

            return false;
        }

        /// <inheritdoc />
        public virtual async Task<IEnumerable<T>> GetAll()
        {
            return await this.repository.GetAll().ToListAsync();
        }
    }
}
