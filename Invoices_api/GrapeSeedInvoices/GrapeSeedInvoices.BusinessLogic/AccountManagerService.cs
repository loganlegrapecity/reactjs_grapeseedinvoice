﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeSeedInvoices.ApiModels;
using GrapeSeedInvoices.BusinessLogic.Common;
using GrapeSeedInvoices.BusinessLogic.Interface;
using GrapeSeedInvoices.DbModels.Entities;
using GrapeSeedInvoices.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace GrapeSeedInvoices.BusinessLogic
{
    public class AccountManagerService : EntityService<AccountManager>, IAccountManagerService
    {
        private readonly IUnitOfWork unitOfWork;

        public AccountManagerService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.AccountManagerRepository)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<List<AccountManager>> SynchronizeData(List<AccountApiModel> accountsFromApi)
        {
            if (accountsFromApi == null || accountsFromApi?.Count == 0)
                return null;

            var accountFromDb = (await this.GetAll())?.ToList();
            if (accountFromDb == null || accountFromDb.Count == 0)
                return await SaveAllList(accountsFromApi);

            foreach (var item in accountsFromApi)
            {
                var isInDb = accountFromDb.Count(x => x.Id == item.Id) > 0;
                if (isInDb)
                    continue;

                await unitOfWork.AccountManagerRepository.Add(new AccountManager()
                {
                    Id = item.Id,
                    Name = item.Name,
                    IsIncludedInReport = true,
                    Email = item.Email
                });
            }

            //delete the item which is not in the API because accountFromAPI is always full
            foreach (var item in accountFromDb)
            {
                var isInApi = accountsFromApi.Count(x => x.Id == item.Id) > 0;
                if(isInApi)
                    continue;

                await this.Delete(item);
            }

            await unitOfWork.CommitAsync();
            return (await this.GetAll()).ToList();
        } 

        public async Task<bool> ToggleIncluded(AccountManager account)
        {
            var dbModel = await unitOfWork.AccountManagerRepository.FindBy(x => x.Id == account.Id).FirstOrDefaultAsync();
            if (dbModel == null)
            {
                await this.Create(account);
                return true;
            }

            dbModel.IsIncludedInReport = account.IsIncludedInReport;
            return await this.Update(dbModel);
        }

        #region Helper

        private async Task<List<AccountManager>> SaveAllList(List<AccountApiModel> accounts)
        {
            foreach (var item in accounts)
            {
                await unitOfWork.AccountManagerRepository.Add(new AccountManager()
                {
                    Id = item.Id,
                    Name = item.Name,
                    IsIncludedInReport = true,
                    Email = item.Email
                });
            }

            await unitOfWork.CommitAsync();
            return (await this.GetAll()).ToList();
        }

        #endregion
    }
}
