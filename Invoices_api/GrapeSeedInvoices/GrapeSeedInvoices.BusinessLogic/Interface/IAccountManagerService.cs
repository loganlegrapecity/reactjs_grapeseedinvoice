﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GrapeSeedInvoices.ApiModels;
using GrapeSeedInvoices.DbModels.Entities;

namespace GrapeSeedInvoices.BusinessLogic.Interface
{
    public interface IAccountManagerService
    {
        Task<IEnumerable<AccountManager>> GetAll();

        Task<List<AccountManager>> SynchronizeData(List<AccountApiModel> accountsFromApi);

        Task<bool> ToggleIncluded(AccountManager account);
    }
}
