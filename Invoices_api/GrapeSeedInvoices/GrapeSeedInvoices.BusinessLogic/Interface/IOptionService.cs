﻿using GrapeSeedInvoices.DbModels.Entities;
using GrapeSeedInvoices.DbModels.Enum;
using System.Threading.Tasks;

namespace GrapeSeedInvoices.BusinessLogic.Interface
{
    public interface IOptionService
    {
        /// <summary>
        /// This method is used to create or update the option, base on the option type
        /// </summary>
        /// <returns></returns>
        Task<Option> Save(OptionType optionType, string value);

        /// <summary>
        /// This method is used to create or update the option, base on the option type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="optionType"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<Option> Save<T>(OptionType optionType, T model);

        Task<T> GetOption<T>(OptionType optionType);

        Task<string> GetOption(OptionType optionType);
    }
}
