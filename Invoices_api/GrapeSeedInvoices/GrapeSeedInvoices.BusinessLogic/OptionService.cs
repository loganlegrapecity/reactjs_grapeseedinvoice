﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GrapeSeedInvoices.BusinessLogic.Common;
using GrapeSeedInvoices.BusinessLogic.Interface;
using GrapeSeedInvoices.DbModels.Entities;
using GrapeSeedInvoices.DbModels.Enum;
using GrapeSeedInvoices.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace GrapeSeedInvoices.BusinessLogic
{
    public class OptionService : EntityService<Option>, IOptionService
    {
        private readonly IUnitOfWork unitOfWork;

        public OptionService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.OptionRepository)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<Option> Save(OptionType optionType, string value)
        {
            var option = await unitOfWork.OptionRepository.FindBy(x => x.OptionType == optionType).FirstOrDefaultAsync();
            if (option == null)
            {
                return await this.Create(new Option()
                {
                    Id = Guid.NewGuid().ToString(),
                    OptionType = optionType,
                    LastUpdate = DateTime.UtcNow,
                    JsonValue = value
                });
            }

            option.JsonValue = value;
            option.LastUpdate = DateTime.UtcNow;
            await this.Update(option);
            return option;
        }

        public async Task<Option> Save<T>(OptionType optionType, T model)
        {
            var json = JsonConvert.SerializeObject(model);
            return await this.Save(optionType, json);
        }

        public async Task<T> GetOption<T>(OptionType optionType)
        {
            var option = await unitOfWork.OptionRepository.FindBy(x => x.OptionType == optionType).FirstOrDefaultAsync();
            if (option == null)
                return default(T);

            return JsonConvert.DeserializeObject<T>(option.JsonValue);
        }

        public async Task<string> GetOption(OptionType optionType)
        {
            return await unitOfWork.OptionRepository
                                   .FindBy(x => x.OptionType == optionType)
                                   .Select(x => x.JsonValue)
                                   .FirstOrDefaultAsync();
        }
    }
}
