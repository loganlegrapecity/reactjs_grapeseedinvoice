﻿using System.ComponentModel.DataAnnotations;

namespace GrapeSeedInvoices.ApiModels
{
    public class LoginApiModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
