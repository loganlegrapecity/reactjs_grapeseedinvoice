﻿namespace GrapeSeedInvoices.ApiModels
{
    public class VietnamLicenseReportSettingApiModel
    {
        public string School { get; set; } = "B";

        public string Campus { get; set; } = "C";

        public string Class { get; set; } = "D";

        public int StartRow { get; set; } = 5;

        public string CurrentUnit { get; set; } = "E";

        public string TotalBilled { get; set; } = "J";

        public string LittleSeed { get; set; } = "K";

        public string TotalAdjustment { get; set; } = "I";

        public int SheetIndex { get; set; } = 2;
    }
}
