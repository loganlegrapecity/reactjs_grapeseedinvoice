﻿using Newtonsoft.Json;
using System;

namespace GrapeSeedInvoices.ApiModels.GenericResponse
{
    public class BoolApiResponse
    {
        public BoolApiResponse(Exception e)
        {
            Success = false;
            ErrorCode = e.HResult;
            Message = e.InnerException?.Message ?? e.Message;
        }

        public BoolApiResponse(bool success)
        {
            Success = success;
        }

        public BoolApiResponse(bool success, string message)
        {
            Success = success;
            Message = message;
        }

        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets message code
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        /// <summary>
        /// Error code
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ErrorCode { get; set; }
    }
}
