﻿using Newtonsoft.Json;
using System;

namespace GrapeSeedInvoices.ApiModels.GenericResponse
{
    public class ApiResponse<T>
    {
        public ApiResponse()
        {
        }

        public ApiResponse(Exception e)
        {
            this.Success = false;
            this.Message = e.Message;
        }

        public ApiResponse(bool success)
        {
            this.Success = success;
        }

        public ApiResponse(T data, bool success)
        {
            this.Data = data;
            this.Success = success;
        }

        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets message code
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets data response
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public T Data { get; set; }
    }
}
