﻿namespace GrapeSeedInvoices.ApiModels
{
    public class ProductApiModel
    {
        public bool disabled { get; set; }

        public string regionEnglishName { get; set; }

        public string regionId { get; set; }

        public string regionName { get; set; }

        public bool regionDisabled { get; set; }

        public string id { get; set; }

        public string name { get; set; }

        public int? startUnit { get; set; }

        public int? endUnit { get; set; }

     //   public string globalProductId { get; set; }

        public string globalProductName { get; set; }

        public bool isTextSubscription { get; set; }

        public bool isDigitalSubscription { get; set; }

        public bool isPurchase { get; set; }
    }
}
