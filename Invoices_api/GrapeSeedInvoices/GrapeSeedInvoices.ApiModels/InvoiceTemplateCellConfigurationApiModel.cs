﻿namespace GrapeSeedInvoices.ApiModels
{
    public class InvoiceTemplateCellConfigurationApiModel
    {
        public int InvoiceDateRow { get; set; } = 6;

        public string InvoiceDateColumn { get; set; } = "B";

        public string SchoolColumn { get; set; } = "B";
        
        public int SchoolRow { get; set; } = 7;

        public string AddressColumn { get; set; } = "B";

        public int AddressRow { get; set; } = 8;

        public int StartFillDataRow { get; set; } = 12;

        public string ProductColumn { get; set; } = "A";

        public string QuantityColumn { get; set; } = "B";

        public string AdjustmentColumn { get; set; } = "C";

        public string PriceColumn { get; set; } = "D";

        public string TotalPriceColumn { get; set; } = "E";

        public int ExchangeRateRow { get; set; } = 10;

        public string ExchangeRateColumn { get; set; } = "E";

        public string SheetName { get; set; } = "Sheet1";
    }
}
