﻿using System.Collections.Generic;

namespace GrapeSeedInvoices.ApiModels
{
    public class SchoolListApiModel
    {
        public List<SchoolApiModel> Schools { get; set; }
    }

    public class SchoolApiModel
    {
        public string id { get; set; }

        public string name { get; set; }

        public string gsNumber { get; set; }

        public string trainerName { get; set; }

        public string campusName { get; set; }

      //  [JsonIgnore]
        public string SchoolAddress { get; set; }

     //   [JsonIgnore]
        public List<string> AccountManagerIds { get; set; }
    }
}
