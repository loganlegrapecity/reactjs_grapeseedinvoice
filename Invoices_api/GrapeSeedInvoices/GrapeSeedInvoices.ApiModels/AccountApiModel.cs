﻿namespace GrapeSeedInvoices.ApiModels
{
    public class AccountApiModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }
    }
}
