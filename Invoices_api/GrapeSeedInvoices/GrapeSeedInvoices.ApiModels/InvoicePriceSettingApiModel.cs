﻿namespace GrapeSeedInvoices.ApiModels
{
    public class InvoicePriceSettingApiModel
    {
        //LittleSEED license price/year
        public double LittleSEEDLicense { get; set; } = 330;

        //GrapeSEED license price/year
        public double GrapeSEEDLicense { get; set; } = 212;

        //Exclude material products from invoices
        public bool ExcludeMaterialProducts { get; set; }
    }
}
