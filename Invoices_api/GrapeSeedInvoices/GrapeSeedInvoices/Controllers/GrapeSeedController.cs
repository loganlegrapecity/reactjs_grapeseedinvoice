﻿using GrapeSeedInvoices.ApiModels;
using GrapeSeedInvoices.ApiModels.GenericResponse;
using GrapeSeedInvoices.CommonAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GrapeSeedInvoices.Controllers
{
    [Route("api/grapeseed")]
    public class GrapeSeedController : GrapeSeedBaseController
    {
        private readonly IGrapeSeedApiService grapeSeedApiService;

        public GrapeSeedController(IGrapeSeedApiService grapeSeedApiService) : base(grapeSeedApiService)
        {
            this.grapeSeedApiService = grapeSeedApiService;
        }

        [Route("schools")]
        public ApiResponse<SchoolListApiModel> GetSchools()
        {
            try
            {
                var data = grapeSeedApiService.GetSchools();
                if (data.Schools?.Count > 0)
                {
                    foreach (var school in data.Schools)
                    {
                        school.AccountManagerIds = grapeSeedApiService.GetAccountIdsOfSchool(school.id);
                        school.SchoolAddress = grapeSeedApiService.GetSchoolAddress(school.id);
                    }
                }

                return new ApiResponse<SchoolListApiModel>(data, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<SchoolListApiModel>(e);
            }
        }

        [Route("accounts")]
        public ApiResponse<List<AccountApiModel>> GetAccounts(string accountIds)
        {
            try
            {
                var data = grapeSeedApiService.GetAccounts(accountIds.Split(",").ToList());
                return new ApiResponse<List<AccountApiModel>>(data, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<List<AccountApiModel>>(e);
            }
        }

        [Route("material-products")]
        public ApiResponse<List<MaterialProductApiModel>> GetMaterialProducts()
        {
            try
            {
                var data = grapeSeedApiService.GetMaterialProducts();
                return new ApiResponse<List<MaterialProductApiModel>>(data, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<List<MaterialProductApiModel>>(e);
            }
        }

        [Route("products")]
        public ApiResponse<List<ProductApiModel>> GetProducts()
        {
            try
            {
                var products = grapeSeedApiService.GetProducts();
                return new ApiResponse<List<ProductApiModel>>(products, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<List<ProductApiModel>>(e);
            }
        }
    }
}