﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using GrapeSeedInvoices.ApiModels.GenericResponse;
using GrapeSeedInvoices.BusinessLogic.Interface;
using GrapeSeedInvoices.DbModels.Enum;

namespace GrapeSeedInvoices.Controllers
{
    [Route("api/upload-file")]
    public class UploadFileController : Controller
    {
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly IOptionService optionService;

        public UploadFileController(IWebHostEnvironment webHostEnvironment, IOptionService optionService)
        {
            this.webHostEnvironment = webHostEnvironment;
            this.optionService = optionService;
        }

        [Route("UploadInvoiceTemplate")]
        [HttpPost]
        public async Task<BoolApiResponse> UploadInvoiceTemplate(List<IFormFile> myFile)
        {
            try
            {
                var uploadedFiles = new List<string>();
                foreach (var formFile in myFile)
                {
                    if (formFile.Length <= 0) continue;

                    var (longPath, shortPath) = GetFilePath(".xlsx");

                    await using var stream = System.IO.File.Create(longPath);
                    await formFile.CopyToAsync(stream);

                    var template = await optionService.Save(OptionType.InvoiceTemplateFilePath, shortPath);
                    if (template != null)
                        uploadedFiles.Add(shortPath);
                }

                return new BoolApiResponse(uploadedFiles?.Count > 0);
            }
            catch (Exception e)
            {
                return new BoolApiResponse(e);
            }
        }

        private Tuple<string, string> GetFilePath(string extension)
        {
            var webRootPath = webHostEnvironment.WebRootPath;
            var newPath = Path.Combine(webRootPath, "uploaded_data");
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            extension = extension.Replace(".", "");
            var fileName = Guid.NewGuid().ToString() + $".{extension}";
            var filePath = Path.Combine(newPath, fileName);

            return new Tuple<string, string>(filePath, "uploaded_data/" + fileName);
        }
    }
}