﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeSeedInvoices.CommonAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace GrapeSeedInvoices.Controllers
{
    public class GrapeSeedBaseController : Controller
    {
        private readonly IGrapeSeedApiService grapeSeedApiService;

        public GrapeSeedBaseController(IGrapeSeedApiService grapeSeedApiService)
        {
            this.grapeSeedApiService = grapeSeedApiService;
        }

        public override void OnActionExecuting(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext context)
        {
            this.grapeSeedApiService.Token = GetHeaderValue("auth");
            base.OnActionExecuting(context);
        }

        private string GetHeaderValue(string key)
        {
            return ControllerContext.HttpContext.Request.Headers
                .Where(x => string.Equals(x.Key, key, StringComparison.CurrentCultureIgnoreCase))
                .Select(x => x.Value)
                .FirstOrDefault();
        }
    }
}