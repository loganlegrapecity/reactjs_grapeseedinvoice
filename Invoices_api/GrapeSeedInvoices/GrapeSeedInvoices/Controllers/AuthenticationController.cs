﻿using GrapeSeedInvoices.CommonAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using GrapeSeedInvoices.ApiModels;

namespace GrapeSeedInvoices.Controllers
{
    [Route("api/authentication")]
    public class AuthenticationController : Controller
    {
        private readonly IAuthenticationApiService authenticationApiService;

        public AuthenticationController(IAuthenticationApiService authenticationApiService)
        {
            this.authenticationApiService = authenticationApiService;
        }

        [Route("sign-in")]
        [HttpPost]
        public async Task<TokenResponse> SignIn([FromBody]LoginApiModel model)
        { 
            var result = await authenticationApiService.Login(model);
            return result;
        }
    }
}