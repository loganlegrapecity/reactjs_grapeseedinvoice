﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapeSeedInvoices.ApiModels.GenericResponse;
using GrapeSeedInvoices.BusinessLogic.Interface;
using GrapeSeedInvoices.CommonAPI.Interfaces;
using GrapeSeedInvoices.DbModels.Entities;
using Microsoft.AspNetCore.Mvc;

namespace GrapeSeedInvoices.Controllers
{
    [Route("api/account-manager")]
    public class AccountManagerController : GrapeSeedBaseController
    {
        private readonly IAccountManagerService accountManagerService;
        private readonly IGrapeSeedApiService grapeSeedApiService;

        public AccountManagerController(IAccountManagerService accountManagerService, IGrapeSeedApiService grapeSeedApiService) : base(grapeSeedApiService)
        {
            this.accountManagerService = accountManagerService;
            this.grapeSeedApiService = grapeSeedApiService;
        }

        [Route("all")]
        public async Task<ApiResponse<List<AccountManager>>> GetAll(string accountManagerIds)
        {
            try
            {
                var data = (await accountManagerService.GetAll())?.ToList();
                if (data == null || data?.Count() == 0)
                {
                    data = await SynchronizeAccounts(accountManagerIds);
                }

                return new ApiResponse<List<AccountManager>>(data, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<List<AccountManager>>(e);
            }
        }

        [Route("synchronize-data")]
        public async Task<ApiResponse<List<AccountManager>>> SynchronizeData(string accountManagerIds)
        {
            try
            {
                var accounts = await SynchronizeAccounts(accountManagerIds);

                return new ApiResponse<List<AccountManager>>(accounts, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<List<AccountManager>>(e);
            }
        }

        [HttpPost]
        [Route("toggle-included")]
        public async Task<BoolApiResponse> ToggleIncluded([FromBody]AccountManager account)
        {
            try
            {
                var success = await accountManagerService.ToggleIncluded(account);
                return new BoolApiResponse(success);
            }
            catch (Exception e)
            {
                return new BoolApiResponse(e);
            }
        }

        private async Task<List<AccountManager>> SynchronizeAccounts(string accountManagerIds)
        {
            var ids = accountManagerIds.Split(",").Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            var accountsResponse = grapeSeedApiService.GetAccounts(ids);
            var accounts = await accountManagerService.SynchronizeData(accountsResponse);
            return accounts;
        }
    }
}