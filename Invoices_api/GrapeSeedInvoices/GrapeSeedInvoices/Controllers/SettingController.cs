﻿using GrapeSeedInvoices.ApiModels;
using GrapeSeedInvoices.ApiModels.GenericResponse;
using GrapeSeedInvoices.BusinessLogic.Interface;
using GrapeSeedInvoices.DbModels.Enum;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace GrapeSeedInvoices.Controllers
{
    [Route("api/setting")]
    public class SettingController : Controller
    {
        private readonly IOptionService optionService;

        public SettingController(IOptionService optionService)
        {
            this.optionService = optionService;
        }

        [HttpGet]
        [Route("invoice-template")]
        public async Task<ApiResponse<string>> GetTemplateFilePath()
        {
            try
            {
                var str = await optionService.GetOption(OptionType.InvoiceTemplateFilePath);
                return new ApiResponse<string>(str, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<string>(e);
            }
        }

        #region Price setting

        [HttpPost]
        [Route("save-price-setting")]
        public async Task<BoolApiResponse> SavePriceSetting([FromBody] InvoicePriceSettingApiModel model)
        {
            try
            {
                var option = await optionService.Save(OptionType.PriceSetting, model);
                return new BoolApiResponse(option != null);
            }
            catch (Exception e)
            {
                return new BoolApiResponse(e);
            }
        }

        [Route("price-setting")]
        public async Task<ApiResponse<InvoicePriceSettingApiModel>> GetPriceSetting()
        {
            try
            {
                var option = await optionService.GetOption<InvoicePriceSettingApiModel>(OptionType.PriceSetting);
                if (option == null)
                    option = new InvoicePriceSettingApiModel();

                return new ApiResponse<InvoicePriceSettingApiModel>(option, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<InvoicePriceSettingApiModel>(e);
            }
        }

        #endregion

        #region Vietnam region license report setting

        [Route("license-report-setting")]
        public async Task<ApiResponse<VietnamLicenseReportSettingApiModel>> GetLicenseReportSetting()
        {
            try
            {
                var option = await optionService.GetOption<VietnamLicenseReportSettingApiModel>(OptionType.VietNamRegionLicenseReport);
                if (option == null)
                    option = new VietnamLicenseReportSettingApiModel();

                return new ApiResponse<VietnamLicenseReportSettingApiModel>(option, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<VietnamLicenseReportSettingApiModel>(e);
            }
        }

        [HttpPost]
        [Route("reset-license-report-setting")]
        public async Task<ApiResponse<VietnamLicenseReportSettingApiModel>> ResetLicenseReportSetting()
        {
            try
            {
                var model = new VietnamLicenseReportSettingApiModel();
                var option = await optionService.Save(OptionType.VietNamRegionLicenseReport, model);
                return new ApiResponse<VietnamLicenseReportSettingApiModel>(model, option != null);
            }
            catch (Exception e)
            {
                return new ApiResponse<VietnamLicenseReportSettingApiModel>(e);
            }
        }

        [HttpPost]
        [Route("save-license-report-setting")]
        public async Task<BoolApiResponse> SaveLicenseReportSetting([FromBody] VietnamLicenseReportSettingApiModel model)
        {
            try
            {
                var option = await optionService.Save(OptionType.VietNamRegionLicenseReport, model);
                return new BoolApiResponse(option != null);
            }
            catch (Exception e)
            {
                return new BoolApiResponse(e);
            }
        }

        #endregion

        #region Cell configuration

        [Route("save-cell-configuration")]
        [HttpPost]
        public async Task<BoolApiResponse> SaveCellConfiguration([FromBody] InvoiceTemplateCellConfigurationApiModel model)
        {
            try
            {
                var option = await optionService.Save(OptionType.InvoiceTemplateCellConfiguration, model);
                return new BoolApiResponse(option != null);
            }
            catch (Exception e)
            {
                return new BoolApiResponse(e);
            }
        }

        [HttpGet]
        [Route("cell-configuration")]
        public async Task<ApiResponse<InvoiceTemplateCellConfigurationApiModel>> GetCellConfiguration()
        {
            try
            {
                var data = await optionService.GetOption<InvoiceTemplateCellConfigurationApiModel>(OptionType.InvoiceTemplateCellConfiguration);
                if (data == null)
                    data = new InvoiceTemplateCellConfigurationApiModel();

                return new ApiResponse<InvoiceTemplateCellConfigurationApiModel>(data, true);
            }
            catch (Exception e)
            {
                return new ApiResponse<InvoiceTemplateCellConfigurationApiModel>(e);
            }
        }

        [Route("reset-cell-configuration")]
        public async Task<BoolApiResponse> ResetCellConfiguration()
        {
            try
            {
                var setting = await optionService.Save(OptionType.InvoiceTemplateCellConfiguration, new InvoiceTemplateCellConfigurationApiModel());
                return new BoolApiResponse(setting != null);
            }
            catch (Exception e)
            {
                return new BoolApiResponse(e);
            }
        }

        #endregion 
    }
}