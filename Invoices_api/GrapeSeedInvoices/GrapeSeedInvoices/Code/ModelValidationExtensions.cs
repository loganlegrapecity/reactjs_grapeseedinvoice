﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Linq;

namespace GrapeSeedInvoices.Code
{
    public static class ModelValidationExtensions
    {
        public static void Validation(this ModelStateDictionary modelState)
        {
            if (!modelState.IsValid)
            {
                var message = string.Join("; ", modelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));

                throw new ArgumentException(message);
            }
        }
    }
}
