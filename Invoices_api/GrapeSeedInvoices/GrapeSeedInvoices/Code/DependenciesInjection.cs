﻿using GrapeSeedInvoices.BusinessLogic;
using GrapeSeedInvoices.BusinessLogic.Interface;
using GrapeSeedInvoices.CommonAPI;
using GrapeSeedInvoices.CommonAPI.Interfaces;
using GrapeSeedInvoices.DbModels;
using GrapeSeedInvoices.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;

namespace GrapeSeedInvoices.Code
{
    public static class DependenciesInjection
    {
        public static void RegisterDependenciesInjection(this IServiceCollection services)
        {
            services.AddScoped<IDbContext, DatabaseContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork.UnitOfWork>();

            services.AddScoped<IAuthenticationApiService, AuthenticationApiService>();
            services.AddScoped<IGrapeSeedApiService, GrapeSeedApiService>();
            services.AddScoped<IOptionService, OptionService>();
            services.AddScoped<IAccountManagerService, AccountManagerService>();
        }
    }
}
